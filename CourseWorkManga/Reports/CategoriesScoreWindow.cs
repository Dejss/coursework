﻿using CourseWorkManga.Connection.Base;
using CourseWorkManga.DataSets;
using Microsoft.Reporting.WinForms;
using System;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace CourseWorkManga.Reports
{
    public partial class CategoriesScoreWindow: Form
    {
        public CategoriesScoreWindow()
        {
            InitializeComponent();
        }

        private void CategoriesScoreWindow_Load(object sender, EventArgs e)
        {
            CategoriesScore categoriesScore = new CategoriesScore();
            SqlDataAdapter adapter = new SqlDataAdapter("EXEC ReportCategoryScore", SQLBinder.SqlConnection);
            adapter.Fill(categoriesScore, categoriesScore.Tables[0].TableName);

            ReportDataSource rds = new ReportDataSource("CategoryScore", categoriesScore.Tables[0]);
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(rds);
            this.reportViewer1.RefreshReport();
        }
    }
}
