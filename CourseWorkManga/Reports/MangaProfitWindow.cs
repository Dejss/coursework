﻿using CourseWorkManga.Connection.Base;
using CourseWorkManga.DataSets;
using Microsoft.Reporting.WinForms;
using System;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace CourseWorkManga.Reports
{
    public partial class MangaProfitWindow: Form
    {
        private DateTime dateStart;
        private DateTime dateEnd;

        public MangaProfitWindow(DateTime start, DateTime end)
        {
            InitializeComponent();
            this.dateStart = start;
            this.dateEnd = end;
        }

        private void MangaProfitWindow_Load(object sender, EventArgs e)
        {
            MangaProfit mangaProfit = new MangaProfit();
            Log.Info(String.Format("EXEC ReportMangaProfit '{0}', '{1}'", this.dateStart.ToString("yyyy-MM-dd"), this.dateEnd.ToString("yyyy-MM-dd")));
            SqlDataAdapter adapter = new SqlDataAdapter(String.Format("EXEC ReportMangaProfit '{0}', '{1}'", this.dateStart.ToString("yyyy-MM-dd"), this.dateEnd.ToString("yyyy-MM-dd")), SQLBinder.SqlConnection);
            adapter.Fill(mangaProfit, mangaProfit.Tables[0].TableName);

            ReportDataSource rds = new ReportDataSource("MangaProfit", mangaProfit.Tables[0]);
            this.reportViewer1.LocalReport.SetParameters(
                new ReportParameter[] {
                    new ReportParameter("dataStart", this.dateStart.ToString("yyyy-MM-dd")),
                    new ReportParameter("dataEnd", this.dateEnd.ToString("yyyy-MM-dd"))
                });
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(rds);
            this.reportViewer1.RefreshReport();
        }
    }
}
