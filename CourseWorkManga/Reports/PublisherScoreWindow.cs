﻿using CourseWorkManga.Connection.Base;
using CourseWorkManga.DataSets;
using Microsoft.Reporting.WinForms;
using System;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace CourseWorkManga.Reports
{
    public partial class PublisherScoreWindow: Form
    {
        public PublisherScoreWindow()
        {
            InitializeComponent();
        }

        private void PublisherScoreWindow_Load(object sender, EventArgs e)
        {
            PublisherScore categoriesScore = new PublisherScore();
            SqlDataAdapter adapter = new SqlDataAdapter("EXEC ReportPublisherScore", SQLBinder.SqlConnection);
            adapter.Fill(categoriesScore, categoriesScore.Tables[0].TableName);

            ReportDataSource rds = new ReportDataSource("PublisherScore", categoriesScore.Tables[0]);
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(rds);
            this.reportViewer1.RefreshReport();
        }
    }
}
