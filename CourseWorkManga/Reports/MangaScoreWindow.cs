﻿using CourseWorkManga.Connection.Base;
using CourseWorkManga.DataSets;
using Microsoft.Reporting.WinForms;
using System;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace CourseWorkManga.Reports
{
    public partial class MangaScoreWindow: Form
    {

        public MangaScoreWindow()
        {
            InitializeComponent();
        }

        private void MangaScore_Load(object sender, EventArgs e)
        {

            MangaScore mangaScore = new MangaScore();
            SqlDataAdapter adapter = new SqlDataAdapter(String.Format("EXEC ReportMangaScore"), SQLBinder.SqlConnection);
            adapter.Fill(mangaScore, mangaScore.Tables[0].TableName);

            ReportDataSource rds = new ReportDataSource("MangaScore", mangaScore.Tables[0]);
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(rds);
            this.reportViewer1.RefreshReport();
        }
    }
}
