﻿namespace CourseWorkManga.Entities
{
    public class Category
    {
        private long id;
        private string title;

        public long Id { get => id; set => id = value; }
        public string Title { get => title; set => title = value; }

        public Category(string title)
        {
            this.title = title;
        }

        public Category(long id, string title)
        {
            this.id = id;
            this.title = title;
        }
    }
}
