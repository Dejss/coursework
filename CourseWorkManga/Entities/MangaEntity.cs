﻿using System;

namespace CourseWorkManga.Entities
{
    public class MangaEntity
    {
        private long        id;
        private String      engTitle;
        private String      jpTitle;
        private String      description;
        private int         tomes;
        private double      score;
        private long        scoredUsers;
        private DateTime    publishDate;
        private DateTime?   publishDateEnd;
        private long        publisherId;
        private int         hide;

        public MangaEntity(string engTitle, double score, int hides)
        {
            this.engTitle = engTitle;
            this.score = score;
            this.hide = hide;
        }

        public MangaEntity(long id, string engTitle, string jpTitle, string description, int tomes, double score, long scoredUsers, DateTime publishDate, DateTime? publishDateEnd, long publisherId, int hide)
        {
            this.id = id;
            this.engTitle = engTitle;
            this.jpTitle = jpTitle;
            this.description = description;
            this.tomes = tomes;
            this.score = score;
            this.scoredUsers = scoredUsers;
            this.publishDate = publishDate;
            this.publishDateEnd = publishDateEnd;
            this.publisherId = publisherId;
            this.Hide = hide;
        }

        public MangaEntity(string engTitle, string jpTitle, string description, int tomes, double score, int scoredUsers, DateTime publishDate, DateTime? publishDateEnd, long publisherId, int hide)
        {
            this.EngTitle = engTitle;
            this.JpTitle = jpTitle;
            this.Description = description;
            this.Tomes = tomes;
            this.Score = score;
            this.ScoredUsers = scoredUsers;
            this.PublishDate = publishDate;
            this.PublishDateEnd = publishDateEnd;
            this.PublisherId = publisherId;
            this.Hide = hide;
        }

        public string EngTitle { get => engTitle; set => engTitle = value; }
        public string JpTitle { get => jpTitle; set => jpTitle = value; }
        public string Description { get => description; set => description = value; }
        public int Tomes { get => tomes; set => tomes = value; }
        public double Score { get => score; set => score = value; }
        public long ScoredUsers { get => scoredUsers; set => scoredUsers = value; }
        public DateTime PublishDate { get => publishDate; set => publishDate = value; }
        public DateTime? PublishDateEnd { get => publishDateEnd; set => publishDateEnd = value; }
        public long PublisherId { get => publisherId; set => publisherId = value; }
        public long Id { get => id; set => id = value; }
        public int Hide { get => hide; set => hide = value; }
    }
}
