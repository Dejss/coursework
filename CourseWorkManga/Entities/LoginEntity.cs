﻿using System;

namespace CourseWorkManga.Entities
{
    class LoginEntity
    {
        private String login;
        private String password;

        public LoginEntity(string login, string password)
        {
            this.login = login;
            this.password = password;
        }

        public string Login { get => login; }
        public string Password { get => password;}
    }
}
