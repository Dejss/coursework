﻿namespace CourseWorkManga.Entities
{
    class AnswerEntity
    {
        public bool isError;
        public string errMessage;

        public AnswerEntity(bool isError, string errMessage)
        {
            this.isError = isError;
            this.errMessage = errMessage;
        }
    }
}
