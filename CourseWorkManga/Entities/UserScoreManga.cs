﻿namespace CourseWorkManga.Entities
{
    class UserScoreManga
    {
        private long id;
        private string userLogin;
        private string mangaTitle;
        private double score;

        public UserScoreManga(long id, string userLogin, string mangaTitle, double score)
        {
            this.id = id;
            this.userLogin = userLogin;
            this.mangaTitle = mangaTitle;
            this.score = score;
        }

        public long Id { get => id; set => id = value; }
        public string UserLogin { get => userLogin; set => userLogin = value; }
        public string MangaTitle { get => mangaTitle; set => mangaTitle = value; }
        public double Score { get => score; set => score = value; }
    }
}
