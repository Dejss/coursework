﻿namespace CourseWorkManga.Entities
{
    public class Chapter
    {
        private long id;
        private int number;
        private string title;
        private long tomeId;

        public Chapter(int number, string title)
        {
            this.number = number;
            this.title = title;
        }

        public Chapter(int number, string title, long tomeId)
        {
            this.number = number;
            this.title = title;
            this.tomeId = tomeId;
        }

        public Chapter(long id, int number, string title, long tomeId)
        {
            this.id = id;
            this.number = number;
            this.title = title;
            this.tomeId = tomeId;
        }

        public long Id { get => id; set => id = value; }
        public int Number { get => number; set => number = value; }
        public string Title { get => title; set => title = value; }
        public long TomeId { get => tomeId; set => tomeId = value; }
    }
}
