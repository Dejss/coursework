﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseWorkManga.Entities
{
    class User
    {
        private long id;
        private string login;

        public User(long id, string login)
        {
            this.Id = id;
            this.Login = login;
        }

        public long Id { get => id; set => id = value; }
        public string Login { get => login; set => login = value; }
    }
}
