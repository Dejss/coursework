﻿namespace CourseWorkManga.Entities
{
    public class PublisherEntity
    {
        private long id;
        private string engTitle;
        private string jpTitle;
        private double score;
        private long scoredUsers;
        private int hide;

        public PublisherEntity(string engTitle, string jpTitle, double score, long scoredUsers, int hide)
        {
            this.engTitle = engTitle;
            this.jpTitle = jpTitle;
            this.score = score;
            this.scoredUsers = scoredUsers;
            this.hide = hide;
        }

        public PublisherEntity(long id, string engTitle, string jpTitle, double score, long scoredUsers, int hide)
        {
            this.id = id;
            this.engTitle = engTitle;
            this.jpTitle = jpTitle;
            this.score = score;
            this.scoredUsers = scoredUsers;
            this.hide = hide;
        }

        public long     Id { get => id; set => id = value; }
        public string   EngTitle { get => engTitle; set => engTitle = value; }
        public string   JpTitle { get => jpTitle; set => jpTitle = value; }
        public double   Score { get => score; set => score = value; }
        public long     ScoredUsers { get => scoredUsers; set => scoredUsers = value; }
        public int Hide { get => hide; set => hide = value; }
    }
}
