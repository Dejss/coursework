﻿namespace CourseWorkManga.Entities
{
    class Page
    {
        private long id;
        private long chapterId;
        private byte[] pageContent;

        public Page(byte[] pageContent)
        {
            this.pageContent = pageContent;
        }

        public Page(long chapterId, byte[] pageContent)
        {
            this.chapterId = chapterId;
            this.pageContent = pageContent;
        }

        public Page(long id, long chapterId, byte[] pageContent)
        {
            this.id = id;
            this.chapterId = chapterId;
            this.pageContent = pageContent;
        }

        public byte[] PageContent { get => pageContent; set => pageContent = value; }
        public long ChapterId { get => chapterId; set => chapterId = value; }
        public long Id { get => id; set => id = value; }
    }
}
