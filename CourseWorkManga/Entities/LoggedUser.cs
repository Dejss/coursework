﻿using System;

namespace CourseWorkManga
{
    public class LoginResult
    {
        private String login;
        private String message;

        public string Login { get => login; set => login = value; }
        public string Message { get => message; set => message = value; }
    }
}
