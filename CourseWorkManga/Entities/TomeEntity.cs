﻿using System.Windows.Media.Imaging;

namespace CourseWorkManga.Entities
{
    class TomeEntity
    {
        private long id;
        private long mangaId;
        private int tomeNumber;
        private int chapters;
        private int price;
        private byte[] preview;
        private int hide;

        public TomeEntity(long mangaId, int hide)
        {
            this.mangaId = mangaId;
            this.hide = hide;
        }

        public TomeEntity()
        {
        }

        public TomeEntity(long mangaId, int tomeNumber, int chapters, int price, byte[] preview, int hide)
        {
            this.mangaId = mangaId;
            this.tomeNumber = tomeNumber;
            this.chapters = chapters;
            this.price = price;
            this.preview = preview;
            this.Hide = hide;
        }

        public TomeEntity(long id, long mangaId, int tomeNumber, int chapters, int price, byte[] preview, int hide)
        {
            this.id = id;
            this.mangaId = mangaId;
            this.tomeNumber = tomeNumber;
            this.chapters = chapters;
            this.price = price;
            this.preview = preview;
            this.Hide = hide;
        }

        public long Id { get => id; set => id = value; }
        public long MangaId { get => mangaId; set => mangaId = value; }
        public int TomeNumber { get => tomeNumber; set => tomeNumber = value; }
        public int Chapters { get => chapters; set => chapters = value; }
        public int Price { get => price; set => price = value; }
        public byte[] Preview { get => preview; set => preview = value; }
        public BitmapImage PreviewImage { get => Utils.ImageConverter.ByteToImage(Preview);}
        public int Hide { get => hide; set => hide = value; }
    }
}
