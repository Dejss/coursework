﻿using System;

namespace CourseWorkManga.Entities
{
    class MangaTomeBuyingService
    {

        private string mangaTitle;
        private int tomeNumber;
        private int price;
        private DateTime date;

        public MangaTomeBuyingService(string mangaTitle, int tomeNumber, int price, DateTime date)
        {
            this.mangaTitle = mangaTitle;
            this.tomeNumber = tomeNumber;
            this.price = price;
            this.date = date;
        }

        public string MangaTitle { get => mangaTitle; set => mangaTitle = value; }
        public int TomeNumber { get => tomeNumber; set => tomeNumber = value; }
        public int Price { get => price; set => price = value; }
        public DateTime Date { get => date; set => date = value; }
    }
}
