﻿namespace CourseWorkManga.Entities
{
    public class UserScorePublisher
    {
        private long id;
        private string userLogin;
        private string publisherTitle;
        private double score;

        public UserScorePublisher(long id, string userLogin, string publisherTitle, double score)
        {
            this.id = id;
            this.userLogin = userLogin;
            this.publisherTitle = publisherTitle;
            this.score = score;
        }

        public long Id { get => id; set => id = value; }
        public string UserLogin { get => userLogin; set => userLogin = value; }
        public string PublisherTitle { get => publisherTitle; set => publisherTitle = value; }
        public double Score { get => score; set => score = value; }
    }
}
