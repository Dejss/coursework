﻿using CourseWorkManga.Connection.Base;
using CourseWorkManga.Entities;
using CourseWorkManga.Utils;

namespace CourseWorkManga.Connection.Login
{
    class LoginInSystem
    {
        public AnswerEntity SingInDatabase(LoginEntity login)
        {
            FileWorker.RemeberMeSave(login);
            return SQLBinder.ConnectToDatabase(login.Login, login.Password);
        }
    }
}
