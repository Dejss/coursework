﻿using CourseWorkManga.Connection.Base;
using CourseWorkManga.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace CourseWorkManga.Connection.Operational
{
    class OperationalDocsRepository
    {
        private static String PROCEDURE_USER_SCORE_MANGA = "MangaScoreHistory";
        private static String PROCEDURE_USER_SCORE_PUBLISHER = "PublisherScoreHistory";
        private static String PROCEDURE_USER_BUYING = "MangaTomeBuyingUser";
        private static String PROCEDURE_SERVICE_BUYING = "MangaTomeBuyingService";

        private static String PROCEDURE_USER_SCORE_MANGA_FILTER = "MangaScoreHistoryFilter";
        private static String PROCEDURE_USER_SCORE_PUBLISHER_FILTER = "PublisherScoreHistoryFilter";
        private static String PROCEDURE_USER_BUYING_FILTER = "MangaTomeBuyingUserFilter";
        private static String PROCEDURE_SERVICE_BUYING_FILTER = "MangaTomeBuyingServiceFilter";

        private static String COLUMN_ID = "id";
        private static String COLUMN_USER_ID = "user_id";
        private static String COLUMN_LOGIN = "login";
        private static String COLUMN_MANGA_TITLE = "manga_title";
        private static String COLUMN_PUBLISHER = "publisher_title";
        private static String COLUMN_TOME_NUMBER = "tome";
        private static String COLUMN_PRICE = "price";
        private static String COLUMN_DATE = "date";
        private static String COLUMN_SCORE = "score";

        public static List<User> GetUsers()
        {
            List<User> history = new List<User>();
            String query = String.Format("select * from USERS");
            SqlCommand sqlCommand = new SqlCommand(query);
            using (var reader = SQLBinder.ExecuteSelectQuery(sqlCommand))
            {
                while (reader.Read())
                {
                    history.Add(new User(
                        Convert.ToInt64(reader[COLUMN_USER_ID].ToString()),
                        reader[COLUMN_LOGIN].ToString()
                    ));
                }
            }
            return history;
        }

        public static List<UserScoreManga> GetUserScoreManga()
        {
            List<UserScoreManga> history = new List<UserScoreManga>();
            String query = String.Format("EXEC {0}", PROCEDURE_USER_SCORE_MANGA);
            SqlCommand sqlCommand = new SqlCommand(query);
            using (var reader = SQLBinder.ExecuteSelectQuery(sqlCommand))
            {
                while (reader.Read())
                {
                    history.Add(new UserScoreManga(
                        Convert.ToInt64(reader[COLUMN_ID].ToString()),
                        reader[COLUMN_LOGIN].ToString(),
                        reader[COLUMN_MANGA_TITLE].ToString(),
                        Convert.ToDouble(reader[COLUMN_SCORE].ToString())
                    ));
                }
            }
            return history;
        }

        public static List<UserScorePublisher> GetUserScorePublisher()
        {
            List<UserScorePublisher> history = new List<UserScorePublisher>();
            String query = String.Format("EXEC {0}", PROCEDURE_USER_SCORE_PUBLISHER);
            SqlCommand sqlCommand = new SqlCommand(query);
            using (var reader = SQLBinder.ExecuteSelectQuery(sqlCommand))
            {
                while (reader.Read())
                {
                    history.Add(new UserScorePublisher(
                        Convert.ToInt64(reader[COLUMN_ID].ToString()),
                        reader[COLUMN_LOGIN].ToString(),
                        reader[COLUMN_PUBLISHER].ToString(),
                        Convert.ToDouble(reader[COLUMN_SCORE].ToString())
                    ));
                }
            }
            return history;
        }

        public static List<MangaTomeBuyingUser> GetMangaTomeBuyingUser()
        {
            List<MangaTomeBuyingUser> history = new List<MangaTomeBuyingUser>();
            String query = String.Format("EXEC {0}", PROCEDURE_USER_BUYING);
            SqlCommand sqlCommand = new SqlCommand(query);
            using (var reader = SQLBinder.ExecuteSelectQuery(sqlCommand))
            {
                while (reader.Read())
                {
                    history.Add(new MangaTomeBuyingUser(
                        reader[COLUMN_LOGIN].ToString(),
                        reader[COLUMN_MANGA_TITLE].ToString(),
                        Convert.ToInt32(reader[COLUMN_TOME_NUMBER].ToString()),
                        Convert.ToInt32(reader[COLUMN_PRICE].ToString()),
                        DateTime.Parse(reader[COLUMN_DATE].ToString())
                    ));
                }
            }
            return history;
        }

        public static List<MangaTomeBuyingService> GetMangaTomeBuyingService()
        {
            List<MangaTomeBuyingService> history = new List<MangaTomeBuyingService>();
            String query = String.Format("EXEC {0}", PROCEDURE_SERVICE_BUYING);
            SqlCommand sqlCommand = new SqlCommand(query);
            using (var reader = SQLBinder.ExecuteSelectQuery(sqlCommand))
            {
                while (reader.Read())
                {
                    history.Add(new MangaTomeBuyingService(
                        reader[COLUMN_MANGA_TITLE].ToString(),
                        Convert.ToInt32(reader[COLUMN_TOME_NUMBER].ToString()),
                        Convert.ToInt32(reader[COLUMN_PRICE].ToString()),
                        DateTime.Parse(reader[COLUMN_DATE].ToString())
                    ));
                }
            }
            return history;
        }


        public static List<UserScoreManga> GetUserScoreMangaFilter(long userid, long mangaid, double score)
        {
            List<UserScoreManga> history = new List<UserScoreManga>();
            SqlCommand sqlCommand = new SqlCommand(PROCEDURE_USER_SCORE_MANGA_FILTER)
            {
                CommandType = CommandType.StoredProcedure
            };
            sqlCommand.Parameters.Add(new SqlParameter("@IdManga", mangaid == -1 ? (object)System.DBNull.Value : mangaid));
            sqlCommand.Parameters.Add(new SqlParameter("@IdUser", userid == -1 ? (object)System.DBNull.Value : userid));
            sqlCommand.Parameters.Add(new SqlParameter("@Score", score == -1 ? (object)System.DBNull.Value : score));
            using (var reader = SQLBinder.ExecuteSelectQuery(sqlCommand))
            {
                while (reader.Read())
                {
                    history.Add(new UserScoreManga(
                        Convert.ToInt64(reader[COLUMN_ID].ToString()),
                        reader[COLUMN_LOGIN].ToString(),
                        reader[COLUMN_MANGA_TITLE].ToString(),
                        Convert.ToDouble(reader[COLUMN_SCORE].ToString())
                    ));
                }
            }
            return history;
        }

        public static List<UserScorePublisher> GetUserScorePublisherFilter(long userid, long publisher, double score)
        {
            List<UserScorePublisher> history = new List<UserScorePublisher>();
            SqlCommand sqlCommand = new SqlCommand(PROCEDURE_USER_SCORE_PUBLISHER_FILTER)
            {
                CommandType = CommandType.StoredProcedure
            };
            sqlCommand.Parameters.Add(new SqlParameter("@IdPublisher", publisher == -1 ? (object)System.DBNull.Value : publisher));
            sqlCommand.Parameters.Add(new SqlParameter("@IdUser", userid == -1 ? (object)System.DBNull.Value : userid));
            sqlCommand.Parameters.Add(new SqlParameter("@Score", score == -1 ? (object)System.DBNull.Value : score));
            using (var reader = SQLBinder.ExecuteSelectQuery(sqlCommand))
            {
                while (reader.Read())
                {
                    history.Add(new UserScorePublisher(
                        Convert.ToInt64(reader[COLUMN_ID].ToString()),
                        reader[COLUMN_LOGIN].ToString(),
                        reader[COLUMN_PUBLISHER].ToString(),
                        Convert.ToDouble(reader[COLUMN_SCORE].ToString())
                    ));
                }
            }
            return history;
        }

        public static List<MangaTomeBuyingUser> GetMangaTomeBuyingUserFilter(long mangaid, long userid, string date)
        {
            List<MangaTomeBuyingUser> history = new List<MangaTomeBuyingUser>();
            SqlCommand sqlCommand = new SqlCommand(PROCEDURE_USER_BUYING_FILTER)
            {
                CommandType = CommandType.StoredProcedure
            };
            sqlCommand.Parameters.Add(new SqlParameter("@IdManga", mangaid == -1 ? (object)System.DBNull.Value : mangaid));
            sqlCommand.Parameters.Add(new SqlParameter("@IdUser", userid == -1 ? (object)System.DBNull.Value : userid));
            sqlCommand.Parameters.Add(new SqlParameter("@Date", date ?? (object)System.DBNull.Value));
            using (var reader = SQLBinder.ExecuteSelectQuery(sqlCommand))
            {
                while (reader.Read())
                {
                    history.Add(new MangaTomeBuyingUser(
                        reader[COLUMN_LOGIN].ToString(),
                        reader[COLUMN_MANGA_TITLE].ToString(),
                        Convert.ToInt32(reader[COLUMN_TOME_NUMBER].ToString()),
                        Convert.ToInt32(reader[COLUMN_PRICE].ToString()),
                        DateTime.Parse(reader[COLUMN_DATE].ToString())
                    ));
                }
            }
            return history;
        }

        public static List<MangaTomeBuyingService> GetMangaTomeBuyingServiceFilter(long id, string date)
        {
            List<MangaTomeBuyingService> history = new List<MangaTomeBuyingService>();
            SqlCommand sqlCommand = new SqlCommand(PROCEDURE_SERVICE_BUYING_FILTER)
            {
                CommandType = CommandType.StoredProcedure
            };
            sqlCommand.Parameters.Add(new SqlParameter("@IdManga", id == -1 ? (object)System.DBNull.Value : id));
            sqlCommand.Parameters.Add(new SqlParameter("@Date", date  ?? (object)System.DBNull.Value));
            using (var reader = SQLBinder.ExecuteSelectQuery(sqlCommand))
            {
                while (reader.Read())
                {
                    history.Add(new MangaTomeBuyingService(
                        reader[COLUMN_MANGA_TITLE].ToString(),
                        Convert.ToInt32(reader[COLUMN_TOME_NUMBER].ToString()),
                        Convert.ToInt32(reader[COLUMN_PRICE].ToString()),
                        DateTime.Parse(reader[COLUMN_DATE].ToString())
                    ));
                }
            }
            return history;
        }

    }
}
