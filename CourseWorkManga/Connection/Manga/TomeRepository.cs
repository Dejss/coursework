﻿using CourseWorkManga.Connection.Base;
using CourseWorkManga.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace CourseWorkManga.Connection.Manga
{
    class TomeChaptersRepository
    {
        private static String PROCEDURE_ADD_TOME = "TomeAdd";
        private static String PROCEDURE_GET_TOME = "TomeGetAll";
        private static String PROCEDURE_GET_FILTER_TOME = "TomeGetFilter";
        private static String PROCEDURE_STATUS_CHANGE_TOME = "TomeStatusChange";

        private static String PROCEDURE_ADD_CHAPTER = "ChaptersAdd";

        private static String INSERT_PAGE_COMMAND = "INSERT INTO MANGA_PAGES (chapter_id, page) VALUES ";

        private static String COLUMN_TOME_ID = "tome_id";
        private static String COLUMN_MANGA_ID = "manga_id";
        private static String COLUMN_TOME_NUMBER = "tome_nubmer";
        private static String COLUMN_CHAPTERS = "chapters";
        private static String COLUMN_PRICE = "price";
        private static String COLUMN_PREVIEW = "preview";
        private static String COLUMN_HIDE = "hide";

        public static long FullSaveTomeChaptersPages(TomeEntity tome, List<Chapter> chapters, List<Page[]> pages)
        {
            long result = -1;
            SqlTransaction transaction = SQLBinder.SqlConnection.BeginTransaction();
            long tomeId = SaveTome(tome, transaction);
            if (tomeId == -1)
                transaction.Rollback();
            else
            {
                tome.Id = tomeId;
                int counter = 0;
                for (int i = 0; i < chapters.Count; i++)
                {
                    long chapterId = SaveChapter(chapters[i], transaction, tome);
                    if (chapterId == -1)
                        break;
                    else
                    {
                        chapters[i].Id = chapterId;
                        counter++;
                    }
                }

                Log.Info("counter - " + counter.ToString());
                Log.Info("chapters - " + chapters.Count.ToString());

                if (counter != chapters.Count)
                    transaction.Rollback();
                else
                {
                    counter = 0;
                    for (int i = 0; i < pages.Count; i++)
                    {
                        long chapterId = SaveChapterPages(pages[i], transaction, chapters[i]);
                        if (chapterId == -1)
                            break;
                        else
                            counter++;
                    }
                    if (counter != pages.Count)
                        transaction.Rollback();
                    else
                    {
                        transaction.Commit();
                        result = tomeId;
                    }
                }
            }
            return result;

        }

        public static long SaveTome(TomeEntity tome, SqlTransaction transaction)
        {
            SqlCommand sqlCommand = new SqlCommand(PROCEDURE_ADD_TOME)
            {
                CommandType = CommandType.StoredProcedure
            };
            if (transaction != null) sqlCommand.Transaction = transaction;
            SqlParameter id = new SqlParameter("@Id", SqlDbType.BigInt)
            {
                Direction = ParameterDirection.Output
            };

            sqlCommand.Parameters.Add(new SqlParameter("@MangaId", tome.MangaId));
            sqlCommand.Parameters.Add(new SqlParameter("@TomeNumber", tome.TomeNumber));
            sqlCommand.Parameters.Add(new SqlParameter("@Chapters", tome.Chapters));
            sqlCommand.Parameters.Add(new SqlParameter("@Price", tome.Price));
            sqlCommand.Parameters.Add(new SqlParameter("@Preview", tome.Preview));
            sqlCommand.Parameters.Add(new SqlParameter("@hide", tome.Hide));
            sqlCommand.Parameters.Add(id);

            if (SQLBinder.ExecuteInsertQuery(sqlCommand) > 0)
            {
                return Convert.ToInt64(id.Value);
            }
            else
            {
                return -1;
            }

        }

        public static long SaveChapter(Chapter chapter, SqlTransaction transaction, TomeEntity tome)
        {
            SqlCommand sqlCommand = new SqlCommand(PROCEDURE_ADD_CHAPTER)
            {
                CommandType = CommandType.StoredProcedure
            };
            if (transaction != null) sqlCommand.Transaction = transaction;
            SqlParameter id = new SqlParameter("@Id", SqlDbType.BigInt)
            {
                Direction = ParameterDirection.Output
            };
            sqlCommand.Parameters.Add(new SqlParameter("@ChapterNumber", chapter.Number));
            sqlCommand.Parameters.Add(new SqlParameter("@Title", chapter.Title));
            sqlCommand.Parameters.Add(new SqlParameter("@TomeId", tome.Id));
            sqlCommand.Parameters.Add(id);

            if (SQLBinder.ExecuteInsertQuery(sqlCommand) > 0)
            {
                return Convert.ToInt64(id.Value);
            }
            else
            {
                return -1;
            }
        }

        public static long SaveChapterPages(Page[] pages, SqlTransaction transaction, Chapter chapter)
        {
            string sqlCommandPages = INSERT_PAGE_COMMAND;
            for (int i = 0; i < pages.Length; i++)
            {
                sqlCommandPages += String.Format("( {0}, @content" + i.ToString() + " ),", chapter.Id);
            }

            SqlCommand sqlCommand = new SqlCommand(sqlCommandPages.Remove(sqlCommandPages.Length - 1));
            if (transaction != null) sqlCommand.Transaction = transaction;

            for (int i = 0; i < pages.Length; i++)
            {
                Log.Info(pages[i].PageContent.GetHashCode() + "");
                sqlCommand.Parameters.AddWithValue("@content" + i.ToString(), pages[i].PageContent);
            }

            if (SQLBinder.ExecuteInsertQuery(sqlCommand) == 0)
            {
                return -1;
            }
            else
            {
                return 1;
            }
        }

        public static List<TomeEntity> GetTome()
        {
            List<TomeEntity> tomes = new List<TomeEntity>();
            SqlCommand sqlCommand = new SqlCommand(PROCEDURE_GET_TOME)
            {
                CommandType = CommandType.StoredProcedure
            };
            using (var reader = SQLBinder.ExecuteSelectQuery(sqlCommand))
            {
                while (reader.Read())
                {
                    long Id = Convert.ToInt64(reader[COLUMN_TOME_ID].ToString());
                    long MangaId = Convert.ToInt64(reader[COLUMN_MANGA_ID].ToString());
                    long TomeNumber = Convert.ToInt32(reader[COLUMN_TOME_NUMBER].ToString());
                    int Chapters = Convert.ToInt32(reader[COLUMN_CHAPTERS].ToString());
                    int Price = Convert.ToInt32(reader[COLUMN_PRICE].ToString());
                    int Hide = Convert.ToInt32(reader[COLUMN_HIDE].ToString());
                    byte[] Preview = (byte[])reader[COLUMN_PREVIEW];
                    tomes.Add(
                        new TomeEntity()
                        {
                            Id = Id, MangaId = MangaId, Chapters = Chapters, Hide = Hide, Price = Price, Preview = Preview

                        }
                    );
                }
            }
            return tomes;
        }

        public static List<TomeEntity> GetTomeFilter(TomeEntity tome)
        {
            List<TomeEntity> tomes = new List<TomeEntity>();
            SqlCommand sqlCommand = new SqlCommand(PROCEDURE_GET_FILTER_TOME)
            {
                CommandType = CommandType.StoredProcedure
            };
            sqlCommand.Parameters.Add(new SqlParameter("@MangaId", tome.MangaId == -1 ? (object)System.DBNull.Value: tome.MangaId));
            sqlCommand.Parameters.Add(new SqlParameter("@Hide", tome.Hide == -1 ? (object)System.DBNull.Value : tome.Hide));
            using (var reader = SQLBinder.ExecuteSelectQuery(sqlCommand))
            {
                while (reader.Read())
                {
                    tomes.Add(
                        new TomeEntity()
                        {
                            Id = Convert.ToInt64(reader[COLUMN_TOME_ID].ToString()),
                            MangaId = Convert.ToInt64(reader[COLUMN_MANGA_ID].ToString()),
                            TomeNumber = Convert.ToInt32(reader[COLUMN_TOME_NUMBER].ToString()),
                            Chapters = Convert.ToInt32(reader[COLUMN_CHAPTERS].ToString()),
                            Price = Convert.ToInt32(reader[COLUMN_PRICE].ToString()),
                            Preview = (byte[])reader[COLUMN_PREVIEW],
                            Hide = Convert.ToInt32(reader[COLUMN_HIDE].ToString())
                        }
                    );
                }
            }
            return tomes;
        }

        public static long ChangeStatusTomea(TomeEntity tome)
        {

            SqlCommand sqlCommand = new SqlCommand(PROCEDURE_STATUS_CHANGE_TOME)
            {
                CommandType = CommandType.StoredProcedure
            };
            sqlCommand.Parameters.Add(new SqlParameter("@Id", tome.Id));
            sqlCommand.Parameters.Add(new SqlParameter("@Hide", tome.Hide));
            if (SQLBinder.ExecuteInsertQuery(sqlCommand) > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}
