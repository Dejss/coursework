﻿using CourseWorkManga.Connection.Base;
using CourseWorkManga.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace CourseWorkManga.Connection.Manga
{
    class CategoryRepository
    {

        private static String PROCEDURE_ADD_CATEGORY = "CategoryAdd";
        private static String PROCEDURE_GET_CATEGORY = "CategoriesGet";
        private static String PROCEDURE_GET_MANGA_CATEGORY = "CategoriesGetManga";
        private static String PROCEDURE_DEL_CATEGORY = "CategoryRemove";
        private static String PROCEDURE_EDT_CATEGORY = "CategoryEdit";

        private static String COLUMN_ID     = "category_id";
        private static String COLUMN_TITLE  = "title";


        public static long SaveCategory(Category category)
        {
            SqlCommand sqlCommand = new SqlCommand(PROCEDURE_ADD_CATEGORY);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add(new SqlParameter("@Title", category.Title));      
            SqlParameter id = new SqlParameter("@Id", SqlDbType.BigInt)
            {
                Direction = ParameterDirection.Output
            };
            sqlCommand.Parameters.Add(id);
            if(SQLBinder.ExecuteInsertQuery(sqlCommand) > 0)
            {
                return Convert.ToInt64(id.Value);
            }
            else
            {
                return 0;
            }
             
        }

        public static List<Category> GetCategories()
        {
            List<Category> categories = new List<Category>();
            String query = String.Format("EXEC {0}", PROCEDURE_GET_CATEGORY);
            SqlCommand sqlCommand = new SqlCommand(query);
            using (var reader = SQLBinder.ExecuteSelectQuery(sqlCommand)) {
                while (reader.Read())
                {
                    long id         = Convert.ToInt64(reader[COLUMN_ID].ToString());
                    string title    = reader[COLUMN_TITLE].ToString();

                    categories.Add(new Category(id, title));
                }               
            }
            return categories;
        }

        public static List<Category> GetCategories(MangaEntity manga)
        {
            List<Category> categories = new List<Category>();
            String query = String.Format("EXEC {0} '{1}'", PROCEDURE_GET_MANGA_CATEGORY, manga.Id);
            SqlCommand sqlCommand = new SqlCommand(query);
            using (var reader = SQLBinder.ExecuteSelectQuery(sqlCommand))
            {
                while (reader.Read())
                {
                    long id = Convert.ToInt64(reader[COLUMN_ID].ToString());
                    string title = reader[COLUMN_TITLE].ToString();

                    categories.Add(new Category(id, title));
                }
            }
            return categories;
        }

        public static int EditCategory(Category category)
        {
           String query = String.Format("EXEC {0} '{1}', '{2}'", PROCEDURE_EDT_CATEGORY, category.Id, category.Title);
           SqlCommand sqlCommand = new SqlCommand(query);
           return SQLBinder.ExecuteInsertQuery(sqlCommand);
        }

        public static int DeletetCategory(Category category)
        {
            String query = String.Format("EXEC {0} '{1}'", PROCEDURE_DEL_CATEGORY, category.Id);
            SqlCommand sqlCommand = new SqlCommand(query);
            return SQLBinder.ExecuteInsertQuery(sqlCommand);
        }
    }
}
