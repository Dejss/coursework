﻿using CourseWorkManga.Connection.Base;
using CourseWorkManga.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace CourseWorkManga.Connection.Manga
{
    class PublisherRepository
    {

        private static String PROCEDURE_GET_PUBLISHER = "PublisherGet";
        private static String PROCEDURE_SAVE_PUBLISHER = "PublisherAdd";
        private static String PROCEDURE_EDIT_PUBLISHER = "PublisherEdit";
        private static String PROCEDURE_STATUS_PUBLISHER = "PublisherStatusChange";
        private static String PROCEDURE_GET_FILTER_PUBLISHER = "PublisherGetFilter";

        private static String COLUMN_ID = "publisher_id";
        private static String COLUMN_ENG_TITLE = "eng_title";
        private static String COLUMN_JP_TITLE = "jp_title";
        private static String COLUMN_SCORE = "score";
        private static String COLUMN_SCORE_USERS = "scored_users";
        private static String COLUMN_HIDE = "hide";

        public static List<PublisherEntity> GetPublishers()
        {
            List<PublisherEntity> publishers = new List<PublisherEntity>();
            String query = String.Format("EXEC {0}", PROCEDURE_GET_PUBLISHER);
            SqlCommand sqlCommand = new SqlCommand(query);
            using (var reader = SQLBinder.ExecuteSelectQuery(sqlCommand))
            {
                while (reader.Read())
                {
                    long id = Convert.ToInt64(reader[COLUMN_ID].ToString());
                    string engTitle = reader[COLUMN_ENG_TITLE].ToString();
                    string jpTitle = reader[COLUMN_JP_TITLE].ToString();
                    double score = Convert.ToDouble(reader[COLUMN_SCORE].ToString());
                    long scoredUsers = Convert.ToInt64(reader[COLUMN_SCORE_USERS].ToString());
                    int hide = Convert.ToInt32(reader[COLUMN_HIDE].ToString());
                    publishers.Add(new PublisherEntity(id, engTitle, jpTitle, score, scoredUsers, hide));
                }
            }
            return publishers;
        }

        public static long SavePublisher (PublisherEntity publisher)
        {

            SqlCommand sqlCommand = new SqlCommand(PROCEDURE_SAVE_PUBLISHER)
            {
                CommandType = CommandType.StoredProcedure
            };
            sqlCommand.Parameters.Add(new SqlParameter("@EngTitle", publisher.EngTitle));
            sqlCommand.Parameters.Add(new SqlParameter("@JpTitle", publisher.JpTitle));
            sqlCommand.Parameters.Add(new SqlParameter("@Score", publisher.Score));
            sqlCommand.Parameters.Add(new SqlParameter("@ScoreUsers", publisher.ScoredUsers));
            SqlParameter id = new SqlParameter("@Id", SqlDbType.BigInt)
            {
                Direction = ParameterDirection.Output
            };
            sqlCommand.Parameters.Add(id);
            if (SQLBinder.ExecuteInsertQuery(sqlCommand) > 0)
            {
                return Convert.ToInt64(id.Value);
            }
            else
            {
                return 0;
            }
        }

        public static long EditPublisher(PublisherEntity publisher)
        {

            SqlCommand sqlCommand = new SqlCommand(PROCEDURE_EDIT_PUBLISHER)
            {
                CommandType = CommandType.StoredProcedure
            };
            sqlCommand.Parameters.Add(new SqlParameter("@Id", publisher.Id));
            sqlCommand.Parameters.Add(new SqlParameter("@EngTitle", publisher.EngTitle));
            if (SQLBinder.ExecuteInsertQuery(sqlCommand) > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public static long ChangeStatusPublisher(PublisherEntity publisher)
        {

            SqlCommand sqlCommand = new SqlCommand(PROCEDURE_STATUS_PUBLISHER)
            {
                CommandType = CommandType.StoredProcedure
            };
            sqlCommand.Parameters.Add(new SqlParameter("@Id", publisher.Id));
            sqlCommand.Parameters.Add(new SqlParameter("@Hide", publisher.Hide));
            if (SQLBinder.ExecuteInsertQuery(sqlCommand) > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public static List<PublisherEntity> GetFilterPublishers(PublisherEntity publisher)
        {
            List<PublisherEntity> publishers = new List<PublisherEntity>();
            SqlCommand sqlCommand = new SqlCommand(PROCEDURE_GET_FILTER_PUBLISHER)
            {
                CommandType = CommandType.StoredProcedure
            };
            sqlCommand.Parameters.Add(new SqlParameter("@Title", publisher.EngTitle ?? (object)System.DBNull.Value));
            sqlCommand.Parameters.Add(new SqlParameter("@Score", publisher.Score == -1 ? (object)System.DBNull.Value : publisher.Score));
            sqlCommand.Parameters.Add(new SqlParameter("@Hide",  publisher.Hide == -1 ? (object)System.DBNull.Value : publisher.Hide));
            using (var reader = SQLBinder.ExecuteSelectQuery(sqlCommand))
            {
                while (reader.Read())
                {
                    long id = Convert.ToInt64(reader[COLUMN_ID].ToString());
                    string engTitle = reader[COLUMN_ENG_TITLE].ToString();
                    string jpTitle = reader[COLUMN_JP_TITLE].ToString();
                    double score = Convert.ToDouble(reader[COLUMN_SCORE].ToString());
                    long scoredUsers = Convert.ToInt64(reader[COLUMN_SCORE_USERS].ToString());
                    int hide = Convert.ToInt32(reader[COLUMN_HIDE].ToString());
                    publishers.Add(new PublisherEntity(id, engTitle, jpTitle, score, scoredUsers, hide));
                }
            }
            return publishers;
        }
        

    }
}
