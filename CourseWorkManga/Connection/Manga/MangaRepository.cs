﻿using CourseWorkManga.Connection.Base;
using CourseWorkManga.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace CourseWorkManga.Connection.Manga
{
    public class MangaRepository
    {
        private static String PROCEDURE_SAVE_MANGA = "MangaAdd";
        private static String PROCEDURE_GET_MANGA = "MangaGet";
        private static String PROCEDURE_GET_FILTER_MANGA = "MangaGetFilters";
        private static String PROCEDURE_EDT_MANGA = "MangaEdit";
        private static String PROCEDURE_STATUS_CHANGE_MANGA = "MangaStatusChange";
        private static string INSERT_CATEGORIES_COMMAND = "INSERT INTO MANGA_CATEGORIES (category_id, manga_id) VALUES ";

        private static String COLUMN_ID = "manga_id";
        private static String COLUMN_ENG_TITLE = "eng_title";
        private static String COLUMN_JP_TITLE = "jp_title";
        private static String COLUMN_DESCRIPTION = "description";
        private static String COLUMN_SCORE = "score";
        private static String COLUMN_SCORE_USERS = "scored_users";
        private static String COLUMN_TOMES = "tomes";
        private static String COLUMN_PUBLISH_DATE = "publish_date";
        private static String COLUMN_PUBLISH_DATE_END = "publish_date_end";
        private static String COLUMN_PUBLISHER_ID = "publisher_id";
        private static String COLUMN_HIDE = "hide";

        public static List<MangaEntity> GetManga()
        {
            List<MangaEntity> categories = new List<MangaEntity>();
            String query = String.Format("EXEC {0}", PROCEDURE_GET_MANGA);
            SqlCommand sqlCommand = new SqlCommand(query);
            using (var reader = SQLBinder.ExecuteSelectQuery(sqlCommand))
            {
                while (reader.Read())
                { 
                    categories.Add(new MangaEntity(
                        Convert.ToInt64(reader[COLUMN_ID].ToString()),
                        reader[COLUMN_ENG_TITLE].ToString(),
                        reader[COLUMN_JP_TITLE].ToString(),
                        reader[COLUMN_DESCRIPTION].ToString(),
                        Convert.ToInt32(reader[COLUMN_TOMES].ToString()),
                        Convert.ToDouble(reader[COLUMN_SCORE].ToString()),
                        Convert.ToInt64(reader[COLUMN_SCORE_USERS].ToString()),
                        DateTime.Parse(reader[COLUMN_PUBLISH_DATE].ToString()),
                        reader[COLUMN_PUBLISH_DATE_END] == System.DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader[COLUMN_PUBLISH_DATE_END].ToString()),
                        Convert.ToInt64(reader[COLUMN_PUBLISHER_ID].ToString()),
                        Convert.ToInt32(reader[COLUMN_HIDE].ToString())
                    ));
                }
            }
            return categories;
        }

        public static List<MangaEntity> GetFilterManga(MangaEntity manga)
        {
            List<MangaEntity> categories = new List<MangaEntity>();
            SqlCommand sqlCommand = new SqlCommand(PROCEDURE_GET_FILTER_MANGA)
            {
                CommandType = CommandType.StoredProcedure
            };
            sqlCommand.Parameters.Add(new SqlParameter("@Title", manga.EngTitle ?? (object)System.DBNull.Value));
            sqlCommand.Parameters.Add(new SqlParameter("@Score", manga.Score == -1 ? (object)System.DBNull.Value : manga.Score));
            sqlCommand.Parameters.Add(new SqlParameter("@Hide", manga.Hide == -1 ? (object)System.DBNull.Value : manga.Hide));           
            using (var reader = SQLBinder.ExecuteSelectQuery(sqlCommand))
            {
                while (reader.Read())
                {
                    categories.Add(new MangaEntity(
                        Convert.ToInt64(reader[COLUMN_ID].ToString()),
                        reader[COLUMN_ENG_TITLE].ToString(),
                        reader[COLUMN_JP_TITLE].ToString(),
                        reader[COLUMN_DESCRIPTION].ToString(),
                        Convert.ToInt32(reader[COLUMN_TOMES].ToString()),
                        Convert.ToDouble(reader[COLUMN_SCORE].ToString()),
                        Convert.ToInt64(reader[COLUMN_SCORE_USERS].ToString()),
                        DateTime.Parse(reader[COLUMN_PUBLISH_DATE].ToString()),
                        reader[COLUMN_PUBLISH_DATE_END] == System.DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader[COLUMN_PUBLISH_DATE_END].ToString()),
                        Convert.ToInt64(reader[COLUMN_PUBLISHER_ID].ToString()),
                        Convert.ToInt32(reader[COLUMN_HIDE].ToString())
                    ));
                }
            }
            Log.Info(categories.Count+"");
            return categories;
        }

        public static long SaveManga(MangaEntity manga, List<Category> categories)
        {
            SqlTransaction transaction = SQLBinder.SqlConnection.BeginTransaction();

            SqlCommand sqlCommand = new SqlCommand(PROCEDURE_SAVE_MANGA)
            {
                CommandType = CommandType.StoredProcedure,
                Transaction = transaction
            };
            sqlCommand.Parameters.Add(new SqlParameter("@EngTitle", manga.EngTitle));
            sqlCommand.Parameters.Add(new SqlParameter("@JpTitle", manga.JpTitle));
            sqlCommand.Parameters.Add(new SqlParameter("@Description", manga.Description));
            sqlCommand.Parameters.Add(new SqlParameter("@Tomes", manga.Tomes));
            sqlCommand.Parameters.Add(new SqlParameter("@Score", manga.Score)); 
            sqlCommand.Parameters.Add(new SqlParameter("@ScoredUsers", manga.ScoredUsers));
            sqlCommand.Parameters.Add(new SqlParameter("@PublishDate", manga.PublishDate.ToString("yyyy-MM-dd")));
            sqlCommand.Parameters.Add(new SqlParameter("@PublishDateEnd", manga.PublishDateEnd == null ? (object) System.DBNull.Value : ((DateTime) manga.PublishDateEnd).ToString("yyyy-MM-dd")));
            sqlCommand.Parameters.Add(new SqlParameter("@PublisherId", manga.PublisherId));
            SqlParameter futureId = new SqlParameter("@Id", SqlDbType.BigInt)
            {
                Direction = ParameterDirection.Output
            };
            sqlCommand.Parameters.Add(futureId);
            long id = -1;
            if (SQLBinder.ExecuteInsertQuery(sqlCommand) < 1)
            {
                transaction.Rollback();
            }
            else
            {
                id = Convert.ToInt64(futureId.Value);
                string sqlCommandCategories = INSERT_CATEGORIES_COMMAND;
                categories.ForEach(item =>
                {
                    sqlCommandCategories += String.Format("({0}, {1}),", item.Id, id);
                });
                sqlCommandCategories = sqlCommandCategories.Remove(sqlCommandCategories.Length - 1, 1) + ";";

                sqlCommand = new SqlCommand(sqlCommandCategories)
                {
                    Transaction = transaction
                };
                if ( SQLBinder.ExecuteInsertQuery(sqlCommand) == 0)
                {
                    transaction.Rollback();
                }
                else
                {
                    transaction.Commit(); 
                }
            }
            return id;
        }

        public static long EditManga(MangaEntity manga, List<Category> categories)
        {
            SqlTransaction transaction = SQLBinder.SqlConnection.BeginTransaction();

            SqlCommand sqlCommand = new SqlCommand(PROCEDURE_EDT_MANGA)
            {
                CommandType = CommandType.StoredProcedure,
                Transaction = transaction
            };
            sqlCommand.Parameters.Add(new SqlParameter("@EngTitle", manga.EngTitle));
            sqlCommand.Parameters.Add(new SqlParameter("@Description", manga.Description));
            sqlCommand.Parameters.Add(new SqlParameter("@PublishDate", manga.PublishDate.ToString("yyyy-MM-dd")));
            sqlCommand.Parameters.Add(new SqlParameter("@PublishDateEnd", manga.PublishDateEnd == null ? (object)System.DBNull.Value : ((DateTime)manga.PublishDateEnd).ToString("yyyy-MM-dd")));
            sqlCommand.Parameters.Add(new SqlParameter("@Id", manga.Id));
            if (SQLBinder.ExecuteInsertQuery(sqlCommand) > 0)
            {

                string dropAll = "delete from MANGA_CATEGORIES WHERE manga_id = " + manga.Id;
                sqlCommand = new SqlCommand(dropAll)
                {
                    Transaction = transaction
                };

                if (SQLBinder.ExecuteInsertQuery(sqlCommand) > 0)
                {
                    string sqlCommandCategories = INSERT_CATEGORIES_COMMAND;
                    categories.ForEach(item =>
                    {
                        sqlCommandCategories += String.Format("({0}, {1}),", item.Id, manga.Id);
                    });
                    sqlCommandCategories = sqlCommandCategories.Remove(sqlCommandCategories.Length - 1, 1) + ";";

                    sqlCommand = new SqlCommand(sqlCommandCategories)
                    {
                        Transaction = transaction
                    };
                    if (SQLBinder.ExecuteInsertQuery(sqlCommand) > 0)
                    {
                        transaction.Commit();
                        return 1;
                    }
                    else
                    {
                        transaction.Rollback();
                        return 0;
                    }
                }
                else
                {
                    transaction.Rollback();
                    return 0;
                }
            }
            else
            {
                transaction.Rollback();
                return 0;
            }
        }

        public static long ChangeStatusManga(MangaEntity manga)
        {

            SqlCommand sqlCommand = new SqlCommand(PROCEDURE_STATUS_CHANGE_MANGA)
            {
                CommandType = CommandType.StoredProcedure
            };
            sqlCommand.Parameters.Add(new SqlParameter("@Id", manga.Id));
            sqlCommand.Parameters.Add(new SqlParameter("@Hide", manga.Hide));
            if (SQLBinder.ExecuteInsertQuery(sqlCommand) > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public static List<MangaEntity> GetMangaFilter(MangaEntity manga)
        {
            List<MangaEntity> mangaList = new List<MangaEntity>();
            SqlCommand sqlCommand = new SqlCommand(PROCEDURE_GET_FILTER_MANGA)
            {
                CommandType = CommandType.StoredProcedure
            };
            sqlCommand.Parameters.Add(new SqlParameter("@Title", manga.EngTitle ?? (object)System.DBNull.Value));
            sqlCommand.Parameters.Add(new SqlParameter("@Score", manga.Score == -1 ? (object)System.DBNull.Value : manga.Score));
            sqlCommand.Parameters.Add(new SqlParameter("@Hide", manga.Hide == -1 ? (object)System.DBNull.Value : manga.Hide));
            using (var reader = SQLBinder.ExecuteSelectQuery(sqlCommand))
            {
                while (reader.Read())
                {
                    mangaList.Add(new MangaEntity(
                        Convert.ToInt64(reader[COLUMN_ID].ToString()),
                        reader[COLUMN_ENG_TITLE].ToString(),
                        reader[COLUMN_JP_TITLE].ToString(),
                        reader[COLUMN_DESCRIPTION].ToString(),
                        Convert.ToInt32(reader[COLUMN_TOMES].ToString()),
                        Convert.ToDouble(reader[COLUMN_SCORE].ToString()),
                        Convert.ToInt64(reader[COLUMN_SCORE_USERS].ToString()),
                        DateTime.Parse(reader[COLUMN_PUBLISH_DATE].ToString()),
                        reader[COLUMN_PUBLISH_DATE_END] == System.DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader[COLUMN_PUBLISH_DATE_END].ToString()),
                        Convert.ToInt64(reader[COLUMN_PUBLISHER_ID].ToString()),
                        Convert.ToInt32(reader[COLUMN_HIDE].ToString())
                    ));
                }
            }
            return mangaList;
        }
    }
}
