﻿using CourseWorkManga.Entities;
using CourseWorkManga.Files;
using CourseWorkManga.Utils;
using System;
using System.Data.SqlClient;

namespace CourseWorkManga.Connection.Base
{
    static class SQLBinder
    {
        private static SqlConnection sqlConnection;

        public static SqlConnection SqlConnection { get => sqlConnection;}

        public static AnswerEntity ConnectToDatabase(String login, String pass)
        {
            ServerEntity server = FileWorker.LoadServerInfo();
            if(server == null)
            {
                return new AnswerEntity(true, "File server.info is broken.");
            }

            string connectionStr =
                String.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3}", server.serverName, server.databaseName, login, pass);

            sqlConnection = new SqlConnection(connectionStr);
            try
            {
                SqlConnection.Open();
            }catch(SqlException e)
            {
                Console.WriteLine(e.StackTrace);
                return new AnswerEntity(true, "Cannot connect to server.");
            }
            return new AnswerEntity(false, null);
        }

        public static bool DisconnectFromDatabase()
        {
            try
            {
                sqlConnection.Close();
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.StackTrace);
                return false;
            }
            return true;
        }

        public static SqlDataReader ExecuteSelectQuery(SqlCommand query)
        {
            query.Connection = sqlConnection;
            return query.ExecuteReader();
        }

        public static int ExecuteInsertQuery(SqlCommand query)
        {
            query.Connection = sqlConnection;
            return query.ExecuteNonQuery();
        }
    }
}
