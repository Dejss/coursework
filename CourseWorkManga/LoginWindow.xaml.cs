﻿using CourseWorkManga.Connection.Login;
using CourseWorkManga.Entities;
using CourseWorkManga.Utils;
using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace CourseWorkManga
{

    public partial class LoginWindow : Window
    {

        public LoginWindow()
        {
            InitializeComponent();
        }

        private void DragWindow(object sender, MouseButtonEventArgs e)
        {
            DragMove();
            Console.WriteLine("Dragging");
        }

        private void CloseWindow(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private  void SignIn(object sender, RoutedEventArgs e)
        {
            AwaitSignIn();
        }

        private async void AwaitSignIn()
        {
            AwaitProgress.Visibility = Visibility.Visible;

            String login = Login.Text;
            String password = Password.Password;

            var progress = new Progress<LoginResult>(userResult =>
            {
                if (userResult.Login == null)
                {
                    new MaterialDialog("Error", userResult.Message).Show();
                    AwaitProgress.Visibility = Visibility.Hidden;
                }
                else
                {
                    MainWindow mainWindow = new MainWindow(userResult);
                    mainWindow.Show();
                    this.Close();
                }
            });
            await Task.Factory.StartNew(() => SingInDataBase(progress, login, password), TaskCreationOptions.LongRunning);
        }

        public void SingInDataBase(IProgress<LoginResult> progress, String login, String password)
        {
            LoginInSystem loginSystem = new LoginInSystem();
            AnswerEntity response = loginSystem.SingInDatabase(new LoginEntity(login, password));

            LoginResult loggedUser = null;
            if (response.isError)
            {
                loggedUser = new LoginResult
                {
                    Login = null,
                    Message = response.errMessage
                };
            } else
            {
                loggedUser = new LoginResult
                {
                    Login = login,
                    Message = null
                };
            }
            progress.Report(loggedUser);
        }

        private void KeyEvent(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Enter:
                    AwaitSignIn();
                    break;
                default:
                    break;
            }
        }
    }
}
