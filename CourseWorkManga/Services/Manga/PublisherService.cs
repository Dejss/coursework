﻿using CourseWorkManga.Connection.Manga;
using CourseWorkManga.Entities;
using CourseWorkManga.Utils;
using System.Collections.Generic;
using System.Windows.Controls;

namespace CourseWorkManga.Services.Manga
{
    class PublisherService
    {

        public List<PublisherEntity> GetPublishers()
        {
            return PublisherRepository.GetPublishers();
        }

        public void SavePublisher(DataGrid mangaTable, PublisherEntity publisher)
        {
            publisher.Id = PublisherRepository.SavePublisher(publisher);
            if (publisher.Id != 0)
            {
                List<PublisherEntity> list = (List<PublisherEntity>)mangaTable.ItemsSource;
                mangaTable.ItemsSource = null;
                list.Add(publisher);
                mangaTable.ItemsSource = list;
            }
            else
            {
                new MaterialDialog("Error", "Cannot add category").Show();
            }
        }

        public void EditPublisher(DataGrid mangaTable, PublisherEntity publisher)
        {
            long result = PublisherRepository.EditPublisher(publisher);
            if (result == 1)
            {
                List<PublisherEntity> list = (List<PublisherEntity>)mangaTable.ItemsSource;
                mangaTable.ItemsSource = null;
                for(int i = 0; i < list.Count; i++)
                {
                    if (list[i].Id == publisher.Id)
                        list.RemoveAt(i);
                }
                list.Add(publisher);
                mangaTable.ItemsSource = list;
            }
            else
            {
                new MaterialDialog("Error", "Cannot add category").Show();
            }
        }

        public void ChangeStatusPublisher(DataGrid mangaTable, PublisherEntity publisher)
        {
            long result = PublisherRepository.ChangeStatusPublisher(publisher);
            if (result == 1)
            {
                List<PublisherEntity> list = (List<PublisherEntity>)mangaTable.ItemsSource;
                mangaTable.ItemsSource = null;
                for (int i = 0; i < list.Count; i++)
                {
                    if (list[i].Id == publisher.Id)
                        list.RemoveAt(i);
                }
                list.Add(publisher);
                mangaTable.ItemsSource = list;
            }
            else
            {
                new MaterialDialog("Error", "Cannot add category").Show();
            }
        }

        public List<PublisherEntity> GetPublishersFilter(PublisherEntity publisher)
        {
            return PublisherRepository.GetFilterPublishers(publisher);
        }

    }
}
