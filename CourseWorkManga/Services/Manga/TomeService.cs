﻿using CourseWorkManga.Connection.Manga;
using CourseWorkManga.Entities;
using CourseWorkManga.Utils;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Controls;

namespace CourseWorkManga.Services.Manga
{
    class TomeService
    {
        private class TomeFile{
            public int TomeNumber;
            public int ChaptersAmount;
            public int FirstChapter;
            public int LastChapter;
            public int Price;
            public List<Chapter> Chapters;
        }

        public void SaveFullTome(string[] files, string[] folders, MangaEntity manga)
        {
            string tomeInfoFile = "";
            string previewFile = "";
            foreach (string filePath in files)
            {
                if (filePath.Contains("tome.info")) tomeInfoFile = filePath;
                else
                if (filePath.Contains("preview")) previewFile = filePath;
            }
            Log.Info("Prepare");
            TomeFile tomeFile = ReadFile(tomeInfoFile);
            Log.Info("File Loaded");
            TomeEntity newTome = PrepareNewFullTome(System.Drawing.Image.FromFile(previewFile), manga, tomeFile);
            List<Chapter> chapters = tomeFile.Chapters;

            List<Entities.Page[]> ChaptersContent = new List<Entities.Page[]>(folders.Length);
            foreach(string folderPath in folders)
                ChaptersContent.Add(ReadChapterContent(folderPath));
            Log.Info("Pages Loaded");
            TomeChaptersRepository.FullSaveTomeChaptersPages(newTome, chapters, ChaptersContent);
            Log.Info("done");
        }

        private TomeEntity PrepareNewFullTome(System.Drawing.Image preview, MangaEntity manga, TomeFile tomeFile)
        {
            return new TomeEntity(
                manga.Id,
                tomeFile.TomeNumber,
                tomeFile.ChaptersAmount,
                tomeFile.Price,
                Utils.ImageConverter.ImageToByte(preview),
                0
            );
        }

        private Entities.Page[] ReadChapterContent(string path)
        {
            string[] files = Directory.GetFiles(path);
            Entities.Page[] pages = new Entities.Page[files.Length];
            for(int i = 0; i < files.Length; i++)
            {
                Log.Info("Reading - " + files[i]);
                pages[i] = new Entities.Page(Utils.ImageConverter.ImageToByte(System.Drawing.Image.FromFile(files[i])));  
            }
            return pages;
        }

        private TomeFile ReadFile(string filePath)
        {
            TomeFile tome = new TomeFile();
            StreamReader reader = new StreamReader(filePath);
            tome.TomeNumber = Convert.ToInt32(reader.ReadLine());
            tome.ChaptersAmount = Convert.ToInt32(reader.ReadLine());
            tome.FirstChapter = Convert.ToInt32(reader.ReadLine());
            tome.LastChapter = Convert.ToInt32(reader.ReadLine());
            tome.Price = Convert.ToInt32(reader.ReadLine());
            tome.Chapters = new List<Chapter>(tome.ChaptersAmount);
            for(int i = 0; i < tome.ChaptersAmount; i++)
            {
                string title = reader.ReadLine();
                Log.Info(title);
                tome.Chapters.Add(new Chapter(tome.FirstChapter + i, title));
            }
            return tome;
        }

        public List<TomeEntity> GetTome()
        {
            List<TomeEntity> asd = TomeChaptersRepository.GetTome();
            return asd;
        }

        public List<TomeEntity> GetTomeFiler(TomeEntity tome)
        {
            return TomeChaptersRepository.GetTomeFilter(tome);
        }

        public void ChangeStatusTome(DataGrid dataGrid, TomeEntity tome)
        {
            long pId = -1;
            foreach (MangaEntity p in MangaRepository.GetManga())
            {
                if (p.Id == tome.MangaId && p.Hide == 1)
                {
                    pId = 1;
                    break;
                }
            }

            if (pId == 1)
            {
                new MaterialDialog("Error", "Manga is hide, cannot change status.").Show();
            }
            else
            {
                long result = TomeChaptersRepository.ChangeStatusTomea(tome);
                if (result == 1)
                {
                    List<TomeEntity> list = (List<TomeEntity>)dataGrid.ItemsSource;
                    dataGrid.ItemsSource = null;
                    for (int i = 0; i < list.Count; i++)
                    {
                        if (list[i].Id == tome.Id)
                            list.RemoveAt(i);
                    }
                    list.Add(tome);
                    dataGrid.ItemsSource = list;
                }
                else
                {
                    new MaterialDialog("Error", "Cannot change status").Show();
                }
            }
        }
    }
}
