﻿using CourseWorkManga.Connection.Manga;
using CourseWorkManga.Entities;
using CourseWorkManga.Utils;
using System.Collections.Generic;
using System.Windows.Controls;

namespace CourseWorkManga.Services.Manga
{
    public class MangaService
    {

        public void SaveManga(DataGrid mangaTable, MangaEntity manga, List<Category> categories)
        {
            manga.Id = MangaRepository.SaveManga(manga, categories);
            if (manga.Id != 0)
            {
                List<MangaEntity> mangaList = (List<MangaEntity>)mangaTable.ItemsSource;
                mangaTable.ItemsSource = null;
                mangaList.Add(manga);
                mangaTable.ItemsSource = mangaList;
            }
            else
            {
                new MaterialDialog("Error", "Cannot add category").Show();
            }
        }

        public List<MangaEntity> GetManga()
        {
            return MangaRepository.GetManga();
        }

        public List<MangaEntity> GetMangaFilter(MangaEntity manga)
        {
            return MangaRepository.GetFilterManga(manga);
        }

        public void EditManga(DataGrid mangaTable, MangaEntity manga, List<Category> categories)
        {
            long result = MangaRepository.EditManga(manga, categories);
            if (result == 1)
            {
                List<MangaEntity> list = (List<MangaEntity>)mangaTable.ItemsSource;
                mangaTable.ItemsSource = null;
                for (int i = 0; i < list.Count; i++)
                {
                    if (list[i].Id == manga.Id)
                        list.RemoveAt(i);
                }
                list.Add(manga);
                mangaTable.ItemsSource = list;
            }
            else
            {
                new MaterialDialog("Error", "Cannot add category").Show();
            }
        }

        public void ChangeStatusManga(DataGrid mangaTable, MangaEntity manga)
        {
            long pId = -1;
            foreach(PublisherEntity p in PublisherRepository.GetPublishers()){
                if(p.Id == manga.PublisherId && p.Hide == 1)
                {
                    pId = 1;
                    break;
                }
            }

            if (pId == 1)
            {
                new MaterialDialog("Error", "Publisher is hide, cannot change status.").Show();
            }
            else
            {
                long result = MangaRepository.ChangeStatusManga(manga);
                if (result == 1)
                {
                    List<MangaEntity> list = (List<MangaEntity>)mangaTable.ItemsSource;
                    mangaTable.ItemsSource = null;
                    for (int i = 0; i < list.Count; i++)
                    {
                        if (list[i].Id == manga.Id)
                            list.RemoveAt(i);
                    }
                    list.Add(manga);
                    mangaTable.ItemsSource = list;
                }
                else
                {
                    new MaterialDialog("Error", "Cannot change status").Show();
                }
            } 
        }
    }
}
