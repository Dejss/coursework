﻿using CourseWorkManga.Connection.Manga;
using CourseWorkManga.Entities;
using CourseWorkManga.Utils;
using System.Collections.Generic;
using System.Windows.Controls;

namespace CourseWorkManga.Services.Manga
{
    class CategoryService
    {
        public void SaveCategory(DataGrid table, Category category)
        {
            category.Id = CategoryRepository.SaveCategory(category);
            if (category.Id != 0)
            {
                List<Category> categories = (List<Category>)table.ItemsSource;
                table.ItemsSource = null;
                categories.Add(category);
                table.ItemsSource = categories;
            }
            else
            {
                new MaterialDialog("Error", "Cannot add category").Show();
            }
        }

        public List<Category> GetCategories()
        {
            return CategoryRepository.GetCategories();
        }

        public List<Category> GetCategoriesManga(MangaEntity manga)
        {
            return CategoryRepository.GetCategories(manga);
        }


        public void EditCategory(Category newCategory)
        {
            CategoryRepository.EditCategory(newCategory);
        }

        public void DeleteCategory(DataGrid table, Category category)
        {
            category.Id = CategoryRepository.DeletetCategory(category);
            if (category.Id != 0)
            {
                List<Category> categories = (List<Category>)table.ItemsSource;
                table.ItemsSource = null;
                categories.Remove(category);
                table.ItemsSource = categories;
            }
            else
            {
                new MaterialDialog("Error", "Cannot delete category").Show();
            }
        }
    }
}
