﻿using CourseWorkManga.Reports;
using System;
using System.Windows;
using System.Windows.Input;

namespace CourseWorkManga.Utils
{
    /// <summary>
    /// Interaction logic for MaterialDialog.xaml
    /// </summary>
    public partial class DateIntervalPicker : Window
    {
        public DateIntervalPicker()
        {
            InitializeComponent();
        }

        private void OKClick(object sender, RoutedEventArgs e)
        {
            if (DateStart.Text.Length == 0 || DateEnd.Text.Length == 0)
                return;
            else
            {
                new MangaProfitWindow(DateStart.SelectedDate.Value, DateEnd.SelectedDate.Value).Show();
            }
            this.Close();
        }

        private void DragOverScreen(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
