﻿using System.Drawing;
using System.IO;
using System.Windows.Media.Imaging;

namespace CourseWorkManga.Utils
{
    class ImageConverter
    {
        public static byte[] ImageToByte(Image image)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                image.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                return stream.ToArray();
            }
        }

        public static BitmapImage ByteToImage(byte[] bytes)
        {
            MemoryStream stream = new MemoryStream(bytes);
            BitmapImage image = new BitmapImage();
            image.BeginInit();
            image.StreamSource = stream;
            image.EndInit();
            return image;
        }

    }
}
