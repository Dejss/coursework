﻿using CourseWorkManga.Entities;
using CourseWorkManga.Files;
using System;
using System.IO;

namespace CourseWorkManga.Utils
{
    class FileWorker
    {
        //==========================
        //      Login Reader
        //==========================
        public static AnswerEntity RemeberMeSave(LoginEntity loginEntity)
        {
            string fileLine = String.Format("{0};{1}", loginEntity.Login, loginEntity.Password);
            if(fileLine.Length >= GetTotalFreeSpace("\\"))
            {
                return new AnswerEntity(true, "No free space");
            }
            try
            {
                File.WriteAllText("user.info", fileLine);
                return new AnswerEntity(false, String.Empty);
            }
            catch
            {
                return new AnswerEntity(true, "Error");
            }            
        }

        public static LoginEntity RemeberMeRestore()
        {
            return null;
        }

        //==========================
        //      Server Reader
        //==========================

        public static ServerEntity LoadServerInfo()
        {
            int i = 0;
            Log.Info("Load server info");
            ServerEntity serverEntity = new ServerEntity();
            string line;
            StreamReader reader = new StreamReader("server.info");
            while ((line = reader.ReadLine()) != null)
            {
                Log.Info(line +" - line");
                if (line.Contains("server"))
                {
                    serverEntity.serverName = line.Replace("server=","");
                }else if (line.Contains("folder"))
                {
                    serverEntity.databaseName = line.Replace("folder=", "");
                }
                i++;
            }
            reader.Close();
            if (i != 2)
            {
                Log.Error("One of param in server.info not founded");
                serverEntity = null;
                return serverEntity;
            }
            else {
                Log.Info("Server = " + serverEntity.serverName);
                Log.Info("Database = " + serverEntity.databaseName);
                return serverEntity;
            }
            
        }

        private static long GetTotalFreeSpace(string driveName)
        {
            foreach (DriveInfo drive in DriveInfo.GetDrives())
            {
                if (drive.IsReady && drive.Name == driveName)
                {
                    Console.WriteLine("Free disk space {0}", drive.TotalFreeSpace);
                    return drive.TotalFreeSpace;
                }
            }
            return -1;
        }
    }
}
