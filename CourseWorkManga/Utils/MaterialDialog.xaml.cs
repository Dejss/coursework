﻿using System;
using System.Windows;
using System.Windows.Input;

namespace CourseWorkManga.Utils
{
    /// <summary>
    /// Interaction logic for MaterialDialog.xaml
    /// </summary>
    public partial class MaterialDialog : Window
    {
        public MaterialDialog(String errTitle, String errText)
        {
            InitializeComponent();
            ErrorText.Text = errText;
            ErrorTitle.Content = errTitle;
        }

        private void OKClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void DragOverScreen(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }
    }
}
