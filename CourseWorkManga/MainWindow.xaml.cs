﻿using CourseWorkManga.Connection.Base;
using CourseWorkManga.MainWin.Screens;
using CourseWorkManga.MainWin.Screens.Operational;
using CourseWorkManga.Reports;
using CourseWorkManga.Utils;
using System.Windows;
using System.Windows.Input;

namespace CourseWorkManga
{
    public partial class MainWindow : Window
    {

        private LoginResult login;

        public MainWindow(LoginResult login)
        {
            InitializeComponent();
            this.login = login;           
            Login.Content = login.Login;
        }

        
        private void LogoutFromSystem()
        {
            SQLBinder.SqlConnection.Close();
            this.Close();
        }

        private void DragOverScreen(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void CloseWindowButtonClick(object sender, RoutedEventArgs e)
        {
            LogoutFromSystem();
            this.Close();
        }

        private void ToolbarMangaClick(object sender, RoutedEventArgs e)
        {
            MangaScreen mangaScreen = new MangaScreen();
            Content.Children.Clear();
            Content.Children.Add(mangaScreen);
        }

        private void Operational_Click(object sender, RoutedEventArgs e)
        {
            OperationalSelector selector = new OperationalSelector();
            Content.Children.Clear();
            Content.Children.Add(selector);
        }

        private void OpenCategoriesScore_Click(object sender, RoutedEventArgs e)
        {
            new CategoriesScoreWindow().Show();
        }

        private void OpenMangaScoreReport_Click(object sender, RoutedEventArgs e)
        {
            new MangaScoreWindow().Show();
        }

        private void OpenPubliserScoreReport_Click(object sender, RoutedEventArgs e)
        {
            new PublisherScoreWindow().Show();
        }

        private void OpenProfitMangaReport_Click(object sender, RoutedEventArgs e)
        {
            new DateIntervalPicker().Show();
        }
    }
}
