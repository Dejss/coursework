﻿using CourseWorkManga.Entities;
using CourseWorkManga.MainWin.Screens.Manga.Dlg;
using CourseWorkManga.Services.Manga;
using CourseWorkManga.Utils;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace CourseWorkManga.MainWin.Screens.Manga
{
    /// <summary>
    /// Interaction logic for MangaSection.xaml
    /// </summary>
    public partial class MangaSection : UserControl, IAddMangaResult, IEditMangaResult
    {
        private MangaService mangaService = new MangaService();

        public MangaSection()
        {
            InitializeComponent();
            LoadTable();
            List<int> avail = new List<int> { 0, 1 };
            List<double> score = new List<double> { 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0 };
            AvailItems.ItemsSource = avail;
            ScoreItems.ItemsSource = score;
        }

        private void LoadTable()
        {
            MangaTable.ItemsSource = mangaService.GetManga();
        }

        private void AddManga(object sender, RoutedEventArgs e)
        {
            (new AddMangaDlg(this)).Show();
        }

        public void AddingResult(MangaEntity manga, List<Category> categories)
        {
            mangaService.SaveManga(MangaTable, manga, categories);
        }

        private void EditRow(object sender, RoutedEventArgs e)
        {
            MangaEntity manga = ((FrameworkElement)sender).DataContext as MangaEntity;
            new EditManga(manga, this).Show();
            
        }

        private void ChangeStatus(object sender, System.Windows.RoutedEventArgs e)
        {
            MangaEntity manga = ((FrameworkElement)sender).DataContext as MangaEntity;
            manga.Hide = manga.Hide == 1 ? 0 : 1;
            mangaService.ChangeStatusManga(MangaTable, manga);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Log.Info(Title.Text);
                string title = Title.Text == "" ? null : Title.Text;

                Log.Info(ScoreItems.Text);
                double score = ScoreItems.Text == "" ? -1 : Convert.ToDouble(ScoreItems.Text);

                Log.Info(AvailItems.Text);
                int hide = AvailItems.Text == "" ? -1 : Convert.ToInt32(AvailItems.Text);

                MangaEntity manga = new MangaEntity(title, score,hide);
                MangaTable.ItemsSource = null;
                MangaTable.ItemsSource = mangaService.GetMangaFilter(manga);

            }
            catch (Exception easd)
            {
                Log.Info(easd.ToString());
                new MaterialDialog("Error", "Check availability to database or what you enter").Show();
            }

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            MangaTable.ItemsSource = null;
            MangaTable.ItemsSource = mangaService.GetManga();
        }

        public void EditResult(MangaEntity manga, List<Category> categories)
        {
            mangaService.EditManga(MangaTable, manga, categories);
        }
    }
}
