﻿using CourseWorkManga.Entities;
using CourseWorkManga.Services.Manga;
using System;
using System.Windows;
using System.Windows.Controls;

namespace CourseWorkManga.MainWin.Screens.Manga
{
    /// <summary>
    /// Логика взаимодействия для Categories.xaml
    /// </summary>
    public partial class Categories : UserControl
    {

        private CategoryService categoryService = new CategoryService();

        public Categories()
        {
            InitializeComponent();
            LoadCategories();
        }

        private void LoadCategories()
        {
            CategoriesTable.ItemsSource = categoryService.GetCategories();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            categoryService.SaveCategory(CategoriesTable, new Category(CategoryTitle.Text)); 
            CategoryTitle.Text = String.Empty;
        }

        private void CategoryTableCellEdit(object sender, DataGridCellEditEndingEventArgs e)
        {
            DataGridRow row = e.Row;
            Category category = row.DataContext as Category;
            TextBox textBox = e.EditingElement as TextBox;

            category.Title = textBox.Text;

            categoryService.EditCategory(category);

        }

        private void DeleteRow(object sender, RoutedEventArgs e)
        {
            Category category = ((FrameworkElement)sender).DataContext as Category;
            categoryService.DeleteCategory(CategoriesTable, category);
        }

    }
}
