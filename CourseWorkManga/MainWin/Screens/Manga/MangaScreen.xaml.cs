﻿using CourseWorkManga.MainWin.Screens.Manga;
using System.Windows;
using System.Windows.Controls;

namespace CourseWorkManga.MainWin.Screens
{
    public partial class MangaScreen : UserControl
    {
        public MangaScreen()
        {
            InitializeComponent();
        }

        private void OpenCategories_Click(object sender, RoutedEventArgs e)
        {
            OpenUserControl(new Categories(), "Categories");
        }

        private void OpenPublishers_Click(object sender, RoutedEventArgs e)
        {
            OpenUserControl(new Publishers(), "Publishers");
        }

        private void OpenManga_Click(object sender, RoutedEventArgs e)
        {
            OpenUserControl(new MangaSection(), "Manga");
        }

        private void OpenTomes_Click(object sender, RoutedEventArgs e)
        {
            OpenUserControl(new Tome(), "Tome");
        }

        private void OpenUserControl(UserControl userControl, string name)
        {
            SelectedCategory.Content = name;
            SubScreen.Children.Clear();
            SubScreen.Children.Add(userControl);
        }
    }
}
