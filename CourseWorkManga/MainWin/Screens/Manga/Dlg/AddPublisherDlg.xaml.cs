﻿using CourseWorkManga.Entities;
using CourseWorkManga.Services.Manga;
using CourseWorkManga.Utils;
using System.Windows;
using System.Windows.Input;

namespace CourseWorkManga.MainWin.Screens.Manga.Dlg
{
    public partial class AddPublisherDlg : Window
    {
        private PublisherService mangaService = new PublisherService();
        private IAddPublisherResult addPublisher;

        public AddPublisherDlg(IAddPublisherResult addPublisher)
        {
            InitializeComponent();
            this.addPublisher = addPublisher;
        }

        private void DragWindow(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void CloseWindow(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Save(object sender, RoutedEventArgs e)
        {
            bool isError = false;
            if (EnglishTitle.Text.Length == 0)
                isError = true;
            else if (JapaneseTitle.Text.Length == 0)
                isError = true;

            if (isError)
            {
                new MaterialDialog("Error", "English & Japanese titles must be filled").Show();
                return;
            }
            addPublisher.AddingResult(new PublisherEntity(EnglishTitle.Text, JapaneseTitle.Text, 0, 0, 1));
            this.Close();
        }
    }
}
