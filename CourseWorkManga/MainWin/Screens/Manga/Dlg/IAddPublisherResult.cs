﻿using CourseWorkManga.Entities;

namespace CourseWorkManga.MainWin.Screens.Manga.Dlg
{
    public interface IAddPublisherResult
    {
        void AddingResult(PublisherEntity publisher);
    }
}
