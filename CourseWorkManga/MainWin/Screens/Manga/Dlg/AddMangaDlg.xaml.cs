﻿using CourseWorkManga.Entities;
using CourseWorkManga.MainWin.Screens.Manga.Content;
using CourseWorkManga.MainWin.Screens.Manga.Dlg;
using CourseWorkManga.Services.Manga;
using CourseWorkManga.Utils;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;

namespace CourseWorkManga.MainWin.Screens.Manga
{
    public partial class AddMangaDlg : Window, IDeleteSelected
    {
        private MangaService mangaService = new MangaService();
        private IAddMangaResult addMangaresult;

        public AddMangaDlg(IAddMangaResult addMangaresult)
        {
            InitializeComponent();
            LoadComboboxes();
            this.addMangaresult = addMangaresult;
        }

        private void DragWindow(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void CloseWindow(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void SaveManga(object sender, RoutedEventArgs e)
        {
            bool isError = false;
            if (EnglishTitle.Text.Length == 0)
                isError = true;
            else if (JapaneseTitle.Text.Length == 0)
                isError = true;
            else if (DateStart.Text.Length == 0)
                isError = true;
            else if (Description.Text.Length == 0)
                isError = true;
            else if (PublishersFill.SelectedItem == null)
                isError = true;

            if (isError)
            {
                new MaterialDialog("Error", "English & Japanese titles, Desription, Publishing date start, Publisher mush be filled and/or selected").Show();
                return;
            }

            MangaEntity manga = new MangaEntity(
                    EnglishTitle.Text,
                    JapaneseTitle.Text,
                    Description.Text,
                    0,
                    0.0,
                    0,
                    DateStart.SelectedDate.Value,
                    DateEnd.SelectedDate,
                    ((PublisherEntity)PublishersFill.SelectedItem).Id,
                    0
            );
            List<Category> categories = new List<Category>();
            
            foreach (UIElement view in MangaCategories.Children)
            {
                categories.Add(((CategoryView) view).Category);
            }
            addMangaresult.AddingResult(manga, categories);
            this.Close();
        }

        private void LoadComboboxes()
        {
            CategoriesFill.ItemsSource = new CategoryService().GetCategories();
            PublishersFill.ItemsSource = new PublisherService().GetPublishers();
        }

        private void AddCategory_Click(object sender, RoutedEventArgs e)
        {
            Category selected = (Category)CategoriesFill.SelectedItem;
            bool good = true;
            foreach (UIElement view in MangaCategories.Children)
            {
                if (selected.Id == ((CategoryView)view).Category.Id)
                    good = false;
            }
            if (selected != null && good)
            {
                CategoryView view = new CategoryView(selected, this);
                MangaCategories.Children.Add(view);
            }
        }

        public void DeleteSelectedCategory(CategoryView categoryView)
        {
            MangaCategories.Children.Remove(categoryView);
        }


    }
}
