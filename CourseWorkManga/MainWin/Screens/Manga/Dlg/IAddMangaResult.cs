﻿using CourseWorkManga.Entities;
using System.Collections.Generic;

namespace CourseWorkManga.MainWin.Screens.Manga.Dlg
{
    public interface IAddMangaResult
    {
        void AddingResult(MangaEntity manga, List<Category> categories);
    }
}
