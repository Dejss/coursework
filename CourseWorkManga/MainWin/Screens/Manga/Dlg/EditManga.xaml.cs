﻿using CourseWorkManga.Entities;
using CourseWorkManga.MainWin.Screens.Manga.Content;
using CourseWorkManga.MainWin.Screens.Manga.Dlg;
using CourseWorkManga.Services.Manga;
using CourseWorkManga.Utils;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;

namespace CourseWorkManga.MainWin.Screens.Manga
{
    public partial class EditManga : Window, IDeleteSelected
    {
        private MangaService mangaService = new MangaService();
        private IEditMangaResult editManga;
        private MangaEntity manga;

        public EditManga(MangaEntity manga, IEditMangaResult editManga)
        {
            InitializeComponent();
            this.manga = manga;
            this.editManga = editManga;
            LoadInfo();
            LoadComboboxes();
        }

        private void LoadInfo()
        {
            EnglishTitle.Text = manga.EngTitle;
            JapaneseTitle.Text = manga.JpTitle;
            Description.Text = manga.Description;
            DateStart.Text = manga.PublishDate.ToString("dd.MM.yyyy");
        }

        private void DragWindow(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void CloseWindow(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void SaveManga(object sender, RoutedEventArgs e)
        {
            bool isError = false;
            if (EnglishTitle.Text.Length == 0)
                isError = true;
            else if (DateStart.Text.Length == 0)
                isError = true;
            else if (Description.Text.Length == 0)
                isError = true;
            else if (PublishersFill.SelectedItem == null)
                isError = true;

            if (isError)
            {
                new MaterialDialog("Error", "English & Japanese titles, Desription, Publishing date start, Publisher mush be filled and/or selected").Show();
                return;
            }

            manga.EngTitle = EnglishTitle.Text;
            manga.Description = Description.Text;
            manga.PublishDate = DateStart.SelectedDate.Value;
            manga.PublishDateEnd = DateEnd.SelectedDate;
            List<Category> categories = new List<Category>();

            foreach (UIElement view in MangaCategories.Children)
            {
                categories.Add(((CategoryView)view).Category);
            }
            editManga.EditResult(manga, categories);
            this.Close();

        }

        private void LoadComboboxes()
        {
            List<Category> categories = new CategoryService().GetCategoriesManga(manga);
            CategoriesFill.ItemsSource = new CategoryService().GetCategories();
            List<PublisherEntity> publisher = new PublisherService().GetPublishers();
            PublishersFill.ItemsSource = publisher;
            foreach(PublisherEntity p in publisher)
            {
                if (p.Id == manga.PublisherId)
                    PublishersFill.SelectedItem = p;
            }
            foreach (Category p in categories)
            {
                CategoryView view = new CategoryView(p, this);
                MangaCategories.Children.Add(view);
            }

            

        }

        private void AddCategory_Click(object sender, RoutedEventArgs e)
        {
            Category selected = (Category) CategoriesFill.SelectedItem;
            bool good = true;
            foreach (UIElement view in MangaCategories.Children)
            {
                if (selected.Id == ((CategoryView)view).Category.Id)
                    good = false;
            }
            if (selected != null && good)
            {
                CategoryView view = new CategoryView(selected, this);
                MangaCategories.Children.Add(view);
            }
        }

        public void DeleteSelectedCategory(CategoryView categoryView)
        {
            MangaCategories.Children.Remove(categoryView);
        }


    }
}
