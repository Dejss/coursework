﻿using CourseWorkManga.Entities;
using System.Collections.Generic;

namespace CourseWorkManga.MainWin.Screens.Manga.Dlg
{
    public interface IEditMangaResult
    {
        void EditResult(MangaEntity manga, List<Category> categories);
    }
}
