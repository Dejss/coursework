﻿using CourseWorkManga.MainWin.Screens.Manga.Dlg;
using CourseWorkManga.Services.Manga;
using System.Windows.Controls;
using CourseWorkManga.Entities;
using System.Windows;
using System.Collections.Generic;
using System;
using CourseWorkManga.Utils;

namespace CourseWorkManga.MainWin.Screens.Manga
{

    public partial class Publishers : UserControl, IAddPublisherResult
    {
        private PublisherService publisherService = new PublisherService();

        public Publishers()
        {
            InitializeComponent();
            LoadPublishers();
            List<int> avail = new List<int>{0,1};
            List<double> score = new List<double> { 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0 };
            AvailItems.ItemsSource = avail;
            ScoreItems.ItemsSource = score;
        }

        public void AddingResult(PublisherEntity publisher)
        {
            publisherService.SavePublisher(PublishersTable, publisher);
        }

        private void LoadPublishers()
        {
            PublishersTable.ItemsSource = publisherService.GetPublishers();
        }

        private void AddNew(object sender, System.Windows.RoutedEventArgs e)
        {
            new AddPublisherDlg(this).Show();
        }

        private void CategoryTableCellEdit(object sender, DataGridCellEditEndingEventArgs e)
        {
            DataGridRow row = e.Row;
            PublisherEntity publisher = row.DataContext as PublisherEntity;
            TextBox textBox = e.EditingElement as TextBox;

            publisher.EngTitle = textBox.Text;

            publisherService.EditPublisher(PublishersTable, publisher);

        }

        private void ChangeStatus(object sender, System.Windows.RoutedEventArgs e)
        {
            PublisherEntity publisher = ((FrameworkElement)sender).DataContext as PublisherEntity;
            publisher.Hide = publisher.Hide == 1 ? 0 : 1;
            publisherService.ChangeStatusPublisher(PublishersTable, publisher);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try {
                Log.Info(Title.Text);
                string title = Title.Text == "" ? null : Title.Text;

                Log.Info(ScoreItems.Text);
                double score = ScoreItems.Text == "" ? -1 : Convert.ToDouble(ScoreItems.Text);

                Log.Info(AvailItems.Text);
                int hide = AvailItems.Text == "" ? -1 : Convert.ToInt32(AvailItems.Text);

                PublisherEntity publisher = new PublisherEntity(title, "", score, 0, hide);
                PublishersTable.ItemsSource = null;
                PublishersTable.ItemsSource = publisherService.GetPublishersFilter(publisher);
            }
            catch(Exception easd)
            {
                Log.Info(easd.ToString());
                new MaterialDialog("Error", "Check availability to database or what you enter").Show();
            }
                 
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            PublishersTable.ItemsSource = null;
            PublishersTable.ItemsSource = publisherService.GetPublishers();
        }
    }
}
