﻿using CourseWorkManga.Entities;
using CourseWorkManga.Services.Manga;
using CourseWorkManga.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Forms;

namespace CourseWorkManga.MainWin.Screens.Manga
{
    /// <summary>
    /// Interaction logic for Tome.xaml
    /// </summary>
    public partial class Tome : System.Windows.Controls.UserControl
    {
        private MangaService mangaService = new MangaService();
        private TomeService tomeService = new TomeService();

        public Tome()
        {
            InitializeComponent();
            LoadTable();
        }

        private void LoadTable()
        {
            MangaFill.ItemsSource = mangaService.GetManga();
            TomeTable.ItemsSource = tomeService.GetTome();
            List<int> avail = new List<int> { 0, 1 };
            AvailItems.ItemsSource = avail;
        }

        private void LoadFullTome_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if(MangaFill.SelectedItem == null)
            {
                new MaterialDialog("Error", "Select manga first").Show();
                return;
            }
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    string[] files = Directory.GetFiles(fbd.SelectedPath);
                    string[] folders = Directory.GetDirectories(fbd.SelectedPath);
                    tomeService.SaveFullTome(files, folders, ((MangaEntity)MangaFill.SelectedItem));
                    TomeTable.ItemsSource = null;
                    TomeTable.ItemsSource = tomeService.GetTome();

                }
            }
        }

        private void ChangeStatus(object sender, System.Windows.RoutedEventArgs e)
        {
            TomeEntity tome = ((FrameworkElement)sender).DataContext as TomeEntity;
            if(tome != null)
            {
                tome.Hide = tome.Hide == 1 ? 0 : 1;
                tomeService.ChangeStatusTome(TomeTable, tome);
            }
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                long id = MangaFill.SelectedItem == null ? -1 : ((MangaEntity)MangaFill.SelectedItem).Id;

                int hide = AvailItems.Text == "" ? -1 : Convert.ToInt32(AvailItems.Text);

                TomeTable.ItemsSource = null;
                TomeTable.ItemsSource = tomeService.GetTomeFiler(new TomeEntity(id, hide));

            }
            catch (Exception easd)
            {
                Log.Info(easd.ToString());
                new MaterialDialog("Error", "Check availability to database or what you enter").Show();
            }

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            TomeTable.ItemsSource = null;
            TomeTable.ItemsSource = tomeService.GetTome();
        }
    }
}
