﻿
namespace CourseWorkManga.MainWin.Screens.Manga.Content
{
    public interface IDeleteSelected
    {
        void DeleteSelectedCategory(CategoryView categoryView);
    }
}
