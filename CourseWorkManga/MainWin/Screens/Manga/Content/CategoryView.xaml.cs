﻿using CourseWorkManga.Entities;
using System.Windows;
using System.Windows.Controls;

namespace CourseWorkManga.MainWin.Screens.Manga.Content
{
    /// <summary>
    /// Interaction logic for Category.xaml
    /// </summary>
    public partial class CategoryView : UserControl 
    {

        private Category category;
        private IDeleteSelected deleteSelected;

        public Category Category { get => category;}

        public CategoryView(Category category, IDeleteSelected deleteSelected)
        {
            InitializeComponent();

            this.deleteSelected = deleteSelected;
            this.category = category;
            this.Title.Content = category.Title;
            this.Width = 45 + category.Title.Length*10;
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            deleteSelected.DeleteSelectedCategory(this);
        }
    }
}
