﻿using CourseWorkManga.Connection.Operational;
using CourseWorkManga.Entities;
using CourseWorkManga.Services.Manga;
using CourseWorkManga.Utils;
using System;
using System.Windows;
using System.Windows.Controls;

namespace CourseWorkManga.MainWin.Screens.Operational
{
    public partial class ServiceBuyingHistory : UserControl
    {
        public ServiceBuyingHistory()
        {
            InitializeComponent();
            LoadContent();
        }

        private void LoadContent()
        {
            OperatinalTable.ItemsSource = OperationalDocsRepository.GetMangaTomeBuyingService();
            MangaFill.ItemsSource = new MangaService().GetManga();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                long id = MangaFill.SelectedItem == null ? -1 : ((MangaEntity)MangaFill.SelectedItem).Id;

                string date = Date.SelectedDate == null ? null : Date.SelectedDate.Value.ToString("yyyy-MM-dd");

                OperatinalTable.ItemsSource = null;
                OperatinalTable.ItemsSource = OperationalDocsRepository.GetMangaTomeBuyingServiceFilter(id, date);

            }
            catch (Exception easd)
            {
                Log.Info(easd.ToString());
                new MaterialDialog("Error", "Check availability to database or what you enter").Show();
            }

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            OperatinalTable.ItemsSource = null;
            OperatinalTable.ItemsSource = OperationalDocsRepository.GetMangaTomeBuyingService();
        }
    }
}
