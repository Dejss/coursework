﻿using System.Windows;
using System.Windows.Controls;

namespace CourseWorkManga.MainWin.Screens.Operational
{
    public partial class OperationalSelector : UserControl
    {
        public OperationalSelector()
        {
            InitializeComponent();
        }

        private void MangaSells_Click(object sender, RoutedEventArgs e)
        {
            OpenUserControl(new UserBuyingHistory(), sender);
        }

        private void MangaScoreHistory_Click(object sender, RoutedEventArgs e)
        {
            OpenUserControl(new UserScoreHistoryManga(), sender);
        }

        private void PublisherScoreHistory_Click(object sender, RoutedEventArgs e)
        {
            OpenUserControl(new UserScoreHistoryPublisher(), sender);
        }

        private void MangaBuying_Click(object sender, RoutedEventArgs e)
        {
            OpenUserControl(new ServiceBuyingHistory(), sender);
        }

        private void OpenUserControl(UserControl userControl, object button)
        {
            SelectedCategory.Content = ((Button)button).Content.ToString();
            SubScreen.Children.Clear();
            SubScreen.Children.Add(userControl);
        }
    }
}
