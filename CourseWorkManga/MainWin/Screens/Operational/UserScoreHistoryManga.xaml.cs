﻿using CourseWorkManga.Connection.Operational;
using CourseWorkManga.Entities;
using CourseWorkManga.Services.Manga;
using CourseWorkManga.Utils;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace CourseWorkManga.MainWin.Screens.Operational
{
    public partial class UserScoreHistoryManga : UserControl
    {
        public UserScoreHistoryManga()
        {
            InitializeComponent();
            LoadContent();
        }

        private void LoadContent()
        {
            MangaFill.ItemsSource = new MangaService().GetManga();
            UserFill.ItemsSource = OperationalDocsRepository.GetUsers();
            List<double> score = new List<double> { 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0 };
            ScoreItems.ItemsSource = score;
            OperatinalTable.ItemsSource = OperationalDocsRepository.GetUserScoreManga();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                long managId = MangaFill.SelectedItem == null ? -1 : ((MangaEntity)MangaFill.SelectedItem).Id;

                long usersid = UserFill.SelectedItem == null ? -1 : ((User)UserFill.SelectedItem).Id;

                double score = ScoreItems.Text == "" ? -1 : Convert.ToDouble(ScoreItems.Text);

                OperatinalTable.ItemsSource = null;
                OperatinalTable.ItemsSource = OperationalDocsRepository.GetUserScoreMangaFilter(usersid, managId, score);

            }
            catch (Exception easd)
            {
                Log.Info(easd.ToString());
                new MaterialDialog("Error", "Check availability to database or what you enter").Show();
            }

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            OperatinalTable.ItemsSource = null;
            OperatinalTable.ItemsSource = OperationalDocsRepository.GetMangaTomeBuyingService();
        }
    }
}
