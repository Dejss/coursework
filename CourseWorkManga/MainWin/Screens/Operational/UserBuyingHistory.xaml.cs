﻿using CourseWorkManga.Connection.Operational;
using CourseWorkManga.Entities;
using CourseWorkManga.Services.Manga;
using CourseWorkManga.Utils;
using System;
using System.Windows;
using System.Windows.Controls;

namespace CourseWorkManga.MainWin.Screens.Operational
{
    /// <summary>
    /// Interaction logic for UserBuyingHistory.xaml
    /// </summary>
    public partial class UserBuyingHistory : UserControl
    {
        public UserBuyingHistory()
        {
            InitializeComponent();
            LoadContent();
        }

        private void LoadContent()
        {
            MangaFill.ItemsSource = new MangaService().GetManga();
            UserFill.ItemsSource = OperationalDocsRepository.GetUsers();
            OperatinalTable.ItemsSource = OperationalDocsRepository.GetMangaTomeBuyingUser();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                long mangaid = MangaFill.SelectedItem == null ? -1 : ((MangaEntity)MangaFill.SelectedItem).Id;

                long usersid = UserFill.SelectedItem == null ? -1 : ((User)UserFill.SelectedItem).Id;

                string date = Date.SelectedDate == null ? null : Date.SelectedDate.Value.ToString("yyyy-MM-dd");

                OperatinalTable.ItemsSource = null;
                OperatinalTable.ItemsSource = OperationalDocsRepository.GetMangaTomeBuyingUserFilter(mangaid, usersid, date);

            }
            catch (Exception easd)
            {
                Log.Info(easd.ToString());
                new MaterialDialog("Error", "Check availability to database or what you enter").Show();
            }

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            OperatinalTable.ItemsSource = null;
            OperatinalTable.ItemsSource = OperationalDocsRepository.GetMangaTomeBuyingService();
        }
    }
}
