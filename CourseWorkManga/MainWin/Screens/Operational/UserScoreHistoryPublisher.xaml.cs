﻿using CourseWorkManga.Connection.Operational;
using CourseWorkManga.Entities;
using CourseWorkManga.Services.Manga;
using CourseWorkManga.Utils;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace CourseWorkManga.MainWin.Screens.Operational
{
    public partial class UserScoreHistoryPublisher : UserControl
    {
        public UserScoreHistoryPublisher()
        {
            InitializeComponent();
            LoadContent();
        }

        private void LoadContent()
        {
            PublisherFill.ItemsSource = new PublisherService().GetPublishers();
            UserFill.ItemsSource = OperationalDocsRepository.GetUsers();
            List<double> score = new List<double> { 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0 };
            ScoreItems.ItemsSource = score;
            OperatinalTable.ItemsSource = OperationalDocsRepository.GetUserScorePublisher();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                long publisherId = PublisherFill.SelectedItem == null ? -1 : ((PublisherEntity)PublisherFill.SelectedItem).Id;

                long usersid = UserFill.SelectedItem == null ? -1 : ((User)UserFill.SelectedItem).Id;

                double score = ScoreItems.Text == "" ? -1 : Convert.ToDouble(ScoreItems.Text);

                OperatinalTable.ItemsSource = null;
                OperatinalTable.ItemsSource = OperationalDocsRepository.GetUserScorePublisherFilter(usersid, publisherId, score);

            }
            catch (Exception easd)
            {
                Log.Info(easd.ToString());
                new MaterialDialog("Error", "Check availability to database or what you enter").Show();
            }

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            OperatinalTable.ItemsSource = null;
            OperatinalTable.ItemsSource = OperationalDocsRepository.GetMangaTomeBuyingService();
        }
    }
}
