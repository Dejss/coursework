use Manga;

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Categories

CREATE PROCEDURE CategoryAdd
  @Title varchar(32),
  @Id int output
AS
BEGIN
  INSERT INTO CATEGORIES (title) values (@Title)
  set @Id=SCOPE_IDENTITY() 
END
GO

CREATE PROCEDURE CategoriesGet
AS
BEGIN
  SELECT * FROM CATEGORIES;
END
GO

CREATE PROCEDURE CategoriesGetManga
	@MangaId int
AS
BEGIN
  select * from CATEGORIES where category_id in (select category_id from MANGA_CATEGORIES where manga_id = @MangaId)
END
GO

CREATE PROCEDURE CategoryEdit
  @Id int,
  @Title varchar(32)
AS
BEGIN
  UPDATE CATEGORIES SET title = @Title WHERE category_id = @Id
END
GO

CREATE PROCEDURE CategoryRemove
  @Id int
AS
BEGIN
  DELETE FROM MANGA_CATEGORIES WHERE category_id = @Id;
  DELETE FROM CATEGORIES WHERE category_id = @Id;
END
GO

--Publisher

CREATE PROCEDURE PublisherAdd
  @EngTitle varchar(256),
  @JpTitle nvarchar(256),
  @Score real,
  @ScoreUsers int,
  @Id int output
AS
BEGIN
  INSERT INTO PUBLISHER_INFO (eng_title, jp_title, score, scored_users) 
  values (@EngTitle, @JpTitle, @Score, @ScoreUsers)
  set @Id=SCOPE_IDENTITY() 
END
GO

CREATE PROCEDURE PublisherGet
AS
BEGIN
  SELECT * FROM PUBLISHER_INFO;
END
GO

CREATE PROCEDURE PublisherGetFilter
	@Title varchar(256),
	@Score real,
	@Hide int
AS
BEGIN
  SELECT * FROM PUBLISHER_INFO where eng_title = ISNULL(@Title, eng_title) and score >= ISNULL(@Score, 0) and hide = ISNULL(@Hide, hide);
END
GO

CREATE PROCEDURE PublisherEdit
  @Id int,
  @EngTitle varchar(256)
AS
BEGIN
  UPDATE PUBLISHER_INFO SET eng_title = @EngTitle where publisher_id = @Id
END
GO

CREATE PROCEDURE PublisherStatusChange
	@Id int,
	@Hide int
AS
BEGIN
	UPDATE PUBLISHER_INFO set hide = @Hide where publisher_id = @Id;
	UPDATE MANGA_INFO set hide = @Hide where publisher_id = @Id;
	update MANGA_TOMES set 
	hide = @Hide
	where manga_id in (
	select manga_id from MANGA_INFO where publisher_id in (select publisher_id from PUBLISHER_INFO where publisher_id = @Id));
END
GO

--Manga

CREATE PROCEDURE MangaAdd

  @EngTitle varchar(256),
  @JpTitle nvarchar(256),
  @Description text,
  @Tomes int,
  @Score real,
  @ScoredUsers int,
  @PublishDate date,
  @PublishDateEnd date,
  @PublisherId int,
  @Id int output
AS
BEGIN
  INSERT INTO MANGA_INFO(eng_title, jp_title, description, tomes, score, scored_users, publish_date, publish_date_end, publisher_id) 
  values 
  (@EngTitle, @JpTitle, @Description, @Tomes, @Score, @ScoredUsers, @PublishDate, @PublishDateEnd, @PublisherId)
  set @Id=SCOPE_IDENTITY() 
END
GO

CREATE PROCEDURE MangaGet
AS
BEGIN
  SELECT * FROM MANGA_INFO;
END
GO

CREATE PROCEDURE MangaGetFilters
	@Title varchar(256),
	@Score real,
	@Hide int
AS
BEGIN
  SELECT * FROM MANGA_INFO where eng_title = ISNULL(@Title, eng_title) and score >= ISNULL(@Score, 0) and hide = ISNULL(@Hide, hide);
END
GO

CREATE PROCEDURE MangaEdit
	@Id int,
	@EngTitle varchar(256),
	@Description text,
	@PublishDate date,
	@PublishDateEnd date
	
AS
BEGIN
	UPDATE MANGA_INFO SET eng_title = @EngTitle, description = @Description, publish_date = @PublishDate, publish_date_end = @PublishDateEnd where manga_id = @Id 
END
GO

CREATE PROCEDURE MangaStatusChange
	@Id int,
	@Hide int
AS
BEGIN
	UPDATE MANGA_INFO set hide = @Hide where manga_id = @Id;
	update MANGA_TOMES set 
	hide = @Hide
	where manga_id in (select manga_id from MANGA_INFO where manga_id = @Id);
END
GO

--Tome

CREATE PROCEDURE TomeAdd

  @MangaId int,
  @TomeNumber int,
  @Chapters int,
  @Price int,
  @Preview image,
  @Hide int,
  @Id int output
AS
BEGIN
	update MANGA_INFO set tomes = tomes + 1 where manga_id = @MangaId;
	INSERT INTO MANGA_TOMES(manga_id, tome_nubmer, chapters, price, preview, hide) 
	values 
	(@MangaId, @TomeNumber, @Chapters, @Price, @Preview, @Hide)
	set @Id=SCOPE_IDENTITY() 
END
GO

CREATE PROCEDURE TomeGetFilter
  @MangaId int,
  @Hide int
AS
BEGIN
  select * from MANGA_TOMES where manga_id = ISNULL(@MangaId,manga_id)  and hide = ISNULL(@Hide, hide);
END
GO


CREATE PROCEDURE TomeGetAll
AS
BEGIN
  select * from MANGA_TOMES
END
GO

CREATE PROCEDURE TomeStatusChange
	@Id int,
	@Hide int
AS
BEGIN
	UPDATE MANGA_TOMES set hide = @Hide where tome_id = @Id;
END
GO

-- Chapters

CREATE PROCEDURE ChaptersAdd

  @ChapterNumber int,
  @TomeId int,
  @Title varchar(256),
  @Id int output
  
AS
BEGIN
  INSERT INTO CHAPTERS_INFO(tome_id, chapter_number, title) 
  values 
  (@TomeId, @ChapterNumber, @Title)
  set @Id=SCOPE_IDENTITY() 
END
GO

-- Operatial Docs

--PublisherScoreHistory
CREATE PROCEDURE PublisherScoreHistory
AS
BEGIN
	select History.id, Users.login, Publishers.eng_title as 'publisher_title', History.score from USERS_SCORE_HISTORY_PUBLISHER as History left join 
	(select * from USERS) as Users on Users.user_id =  History.user_id left join
	(select * from PUBLISHER_INFO) as Publishers on Publishers.publisher_id = History.publisher_id
END
GO

CREATE PROCEDURE PublisherScoreHistoryFilter
	@IdUser int,
    @IdPublisher int,
	@Score real
AS
BEGIN
	select History.id, Users.login, Publishers.eng_title as 'publisher_title', History.score from USERS_SCORE_HISTORY_PUBLISHER as History left join 
	(select * from USERS) as Users on Users.user_id =  History.user_id left join
	(select * from PUBLISHER_INFO) as Publishers on Publishers.publisher_id = History.publisher_id
	where  Publishers.publisher_id = ISNULL(@IdPublisher,Publishers.publisher_id) and Users.user_id = ISNULL(@IdUser,Users.user_id) and History.score >= ISNULL(@Score, 0);
END
GO
--MangaScoreHistory
create PROCEDURE MangaScoreHistory
AS
BEGIN
	select History.id, Users.login, Manga.eng_title as 'manga_title', History.score from USERS_SCORE_HISTORY_MANGA as History left join 
	(select * from USERS) as Users on Users.user_id =  History.user_id left join 
	(select * from MANGA_INFO) as Manga on Manga.manga_id = History.manga_id
END
GO

CREATE PROCEDURE MangaScoreHistoryFilter
    @IdUser int,
    @IdManga int,
	@Score real
AS
BEGIN
	select History.id, Users.login, Manga.eng_title as 'manga_title', History.score from USERS_SCORE_HISTORY_MANGA as History left join 
	(select * from USERS) as Users on Users.user_id =  History.user_id left join 
	(select * from MANGA_INFO) as Manga on Manga.manga_id = History.manga_id
	where Manga.manga_id = ISNULL(@IdManga, Manga.manga_id) and Users.user_id = ISNULL(@IdUser,Users.user_id) and History.score >= ISNULL(@Score, 0);
END
GO

--MangaTomeBuyingUser
CREATE PROCEDURE MangaTomeBuyingUser
AS
BEGIN
	select users.login, manga.eng_title as 'manga_title', tomes.tome_nubmer as 'tome', buying.price, buying.date from MANGA_BUYING_USERS as buying left join
	(select * from USERS) as users on users.user_id = buying.user_id  left join
	(select * from MANGA_TOMES) as tomes on tomes.tome_id = buying.tome_id left join
	(select * from MANGA_INFO) as manga on manga.manga_id = tomes.manga_id
	
END
GO

CREATE PROCEDURE MangaTomeBuyingUserFilter
    @IdUser int,
	@IdManga int,
	@Date date
AS
BEGIN
	select users.login, manga.eng_title as 'manga_title', tomes.tome_nubmer as 'tome', buying.price, buying.date from MANGA_BUYING_USERS as buying left join
	(select * from USERS) as users on users.user_id = buying.user_id  left join
	(select * from MANGA_TOMES) as tomes on tomes.tome_id = buying.tome_id left join
	(select * from MANGA_INFO) as manga on manga.manga_id = tomes.manga_id
	where Manga.manga_id = ISNULL(@IdManga, Manga.manga_id) and Users.user_id = ISNULL(@IdUser,Users.user_id) and buying.date >= ISNULL(@Date, '1970-01-01');
END
GO

--MangaTomeBuyingService
CREATE PROCEDURE MangaTomeBuyingService
AS
BEGIN
	select manga.eng_title as 'manga_title', tomes.tome_nubmer as 'tome', buying.price,buying.date  from MANGA_BUYING_SERVICE as buying left join
	(select * from MANGA_TOMES) as tomes on tomes.tome_id = buying.tome_id left join
	(select * from MANGA_INFO) as manga on manga.manga_id = tomes.manga_id
END
GO

CREATE PROCEDURE MangaTomeBuyingServiceFilter
	@IdManga int,
	@Date date
AS
BEGIN
	select manga.eng_title as 'manga_title', tomes.tome_nubmer as 'tome', buying.price,buying.date  from MANGA_BUYING_SERVICE as buying left join
	(select * from MANGA_TOMES) as tomes on tomes.tome_id = buying.tome_id left join
	(select * from MANGA_INFO) as manga on manga.manga_id = tomes.manga_id
	where Manga.manga_id = ISNULL(@IdManga, Manga.manga_id) and buying.date >= ISNULL(@Date, '1970-01-01');
END
GO

-- Reports

CREATE PROCEDURE ReportMangaProfit

  @DataStart date,
  @DateEnd date
AS
BEGIN
	select MANGA_INFO.eng_title, ISNULL(profit,0) as 'profit' from MANGA_INFO left join 
		(select manga_id, profit from MANGA_TOMES left join 
			(SELECT tome_id, SUM(price) as 'profit' FROM MANGA_BUYING_USERS WHERE date >= @DataStart AND date <= @DateEnd GROUP BY tome_id) 
		as tomeProfit on MANGA_TOMES.tome_id = tomeProfit.tome_id) 
	as tomeInfo on MANGA_INFO.manga_id = tomeInfo.manga_id;
END
GO

CREATE PROCEDURE ReportCategoryScore
AS
BEGIN
	declare @TmpTable table(
		title varchar(256),
		score real
	);
	insert into @TmpTable 
		select title, score from MANGA_INFO left join 
			(select CATEGORIES.category_id, manga_id, CATEGORIES.title from CATEGORIES left join 
				(select * from MANGA_CATEGORIES) 
			as mangaCategories on mangaCategories.category_id = CATEGORIES.category_id) 
		as manga on MANGA_INFO.manga_id = manga.manga_id;

	select title, AVG(score) as 'score' from @TmpTable group by title;
END
GO

CREATE PROCEDURE ReportMangaScore
AS
BEGIN
	select eng_title as 'title', scored_users, score from MANGA_INFO
END
GO

CREATE PROCEDURE ReportPublisherScore
AS
BEGIN
	select eng_title as 'title', scored_users, score from PUBLISHER_INFO
END
GO


