use Manga;

create table PUBLISHER_INFO(
	publisher_id int IDENTITY PRIMARY KEY,
	eng_title varchar(256),
	jp_title nvarchar(256),
	hide int,
	score real,
	scored_users int
);

create table MANGA_INFO(
	manga_id int IDENTITY PRIMARY KEY,
	eng_title varchar(256),
	jp_title nvarchar(256),
	description text,
	tomes int,
	score real,
	scored_users int,
	publish_date date,
	publish_date_end date,
	publisher_id int FOREIGN KEY REFERENCES PUBLISHER_INFO(publisher_id),
	hide int
);

create table MANGA_TOMES(
	tome_id int IDENTITY PRIMARY KEY,
	manga_id int FOREIGN KEY REFERENCES MANGA_INFO(manga_id),
	tome_nubmer int,
	chapters int,
	price int,
	preview image,
	hide int
);

create table CHAPTERS_INFO(
	chapter_id int IDENTITY PRIMARY KEY,
	tome_id int FOREIGN KEY REFERENCES MANGA_TOMES(tome_id),
	chapter_number int,
	title varchar(256)
);

create table CATEGORIES(
	category_id int IDENTITY PRIMARY KEY,
	title varchar(32)
);

create table MANGA_CATEGORIES(
	category_id int FOREIGN KEY REFERENCES CATEGORIES(category_id),
	manga_id int FOREIGN KEY REFERENCES MANGA_INFO(manga_id)
);

create table MANGA_PAGES(
	page_id int IDENTITY PRIMARY KEY,
	chapter_id int FOREIGN KEY REFERENCES CHAPTERS_INFO(chapter_id),
	page image
);

create table USERS(
	user_id int IDENTITY PRIMARY KEY,
	login nvarchar(256),
	password varchar(128),
	email varchar(254),
	wallet int,
);



create table USERS_SCORE_HISTORY_MANGA(
	id int IDENTITY PRIMARY KEY,
	user_id int FOREIGN KEY REFERENCES USERS(user_id),
	manga_id int FOREIGN KEY REFERENCES MANGA_INFO(manga_id),
	score real
);

create table MANGA_BUYING_USERS(
	id int IDENTITY PRIMARY KEY,
	user_id int FOREIGN KEY REFERENCES USERS(user_id),
	tome_id int,
	price int,
	date date
);

create table USERS_SCORE_HISTORY_PUBLISHER(
	id int IDENTITY PRIMARY KEY,
	user_id int FOREIGN KEY REFERENCES USERS(user_id),
	publisher_id int FOREIGN KEY REFERENCES PUBLISHER_INFO(publisher_id),
	score real
);

create table MANGA_BUYING_SERVICE(
	id int IDENTITY PRIMARY KEY,
	manga_id int FOREIGN KEY REFERENCES MANGA_INFO(manga_id),
	tome_id int,
	price int,
	date date
);